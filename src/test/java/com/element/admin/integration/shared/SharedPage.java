package com.element.admin.integration.shared;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

public class SharedPage {
	public WebDriver driver;

	public int DRIVER_WAIT = 30; // 30 seconds

	/**
	 * Constructor.
	 *
	 * @param driver an instance of WebDriver
	 */
	public SharedPage(final WebDriver driver) {
		final ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, DRIVER_WAIT);
		PageFactory.initElements(finder, this);
		this.driver = driver;
	}

	@FindBy(xpath = "//div[4]/div/div[2]/div[1]/div[2]/div/ul/li[1]/button")
	public WebElement webelement_advanceSearch;

	@FindBy(xpath = "//div[4]/div/div[2]/div[2]/div[2]/div/ul/li[1]/button")
	public WebElement webelement_dashboard;

	@FindBy(xpath = "//div[4]/div/div[2]/div[2]/div[2]/div/ul/li[2]/button")
	public WebElement webelement_workSpace;

	@FindBy(xpath = "//div[4]/div/div[2]/div[2]/div[2]/div/ul/li[3]/button")
	public WebElement webelement_investigationConsole;

	@FindBy(xpath = "//div[4]/div/div[2]/div[2]/div[2]/div/ul/li[4]/button")
	public WebElement webelement_transctionMonitoring;

	@FindBy(xpath = "//div[4]/div/div[2]/div[3]/div[2]/div/ul/li/button")
	public WebElement webelement_leadGeneration;

	@FindBy(xpath = "//div[4]/div/div[2]/div[4]/div[2]/div/ul/li[3]/button")
	public WebElement webelement_linkAnalysis;

	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[1]/button")
	public WebElement webelement_orchestration;

	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[2]/button")
	public WebElement webelement_appManager;

	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[3]/button")
	public WebElement webelement_dataManagement;

	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[5]/button")
	public WebElement webelement_bigDataSciencePlatform;

	@FindBy(xpath = "//div[4]/div/div[2]/div[1]/div[2]/div/ul/li[2]/button")
	public WebElement webelement_onboarding;

	@FindBy(xpath = "//div[4]/div/div[2]/div[2]/div[2]/div/ul/li[5]/button")
	public WebElement webelement_marketIntelligence;

	@FindBy(xpath = "//div[4]/div/div[2]/div[4]/div[2]/div/ul/li[1]/button")
	public WebElement webelement_entity;

	@FindBy(xpath = "//div[4]/div/div[2]/div[4]/div[2]/div/ul/li[2]/button")
	public WebElement webelement_cases;

	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[4]/button")
	public WebElement webelement_systemMonitoring;

	@FindBy(xpath = "//div[4]/div/div[2]/div[2]/div[2]/div/ul/li[6]/button")
	public WebElement webelement_adverseTransaction;

	@FindBy(xpath = "//div[4]/div/div[2]/div[2]/div[2]/div/ul/li[7]/button")
	public WebElement webelement_fraud;

	@FindBy(xpath = "//div[4]/div/div[2]/div[4]/div[2]/div/ul/li[4]/button")
	public WebElement webelement_underwriting;

	@FindBy(xpath = "//div[4]/div/div[2]/div[4]/div[2]/div/ul/li[5]/button")
	public WebElement webelement_auditTrail;

	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[6]/button")
	public WebElement webelement_systemSettings;

	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[7]/button")
	public WebElement webelement_policyEnforcement;

	@FindBy(xpath = "//div[4]/div/div[2]/div[2]/div[2]/div/ul/li[8]/button")
	public WebElement webelement_transactionIntelligence;

	@FindBy(xpath = "//div[4]/div/div[2]/div[2]/div[2]/div/ul/li[9]/button")
	public WebElement webelement_eDiscover;

	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[8]/button")
	public WebElement webelement_questionaryBuilder;

	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[9]/button")
	public WebElement webelement_datacuration;

	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[10]/button")
	public WebElement webelement_decisionScoring;

	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[11]/button")
	public WebElement webelement_documentParsing;

	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[12]/button")
	public WebElement webelement_sourceManagement;

	public void clickAdvanceSearch() {

		webelement_advanceSearch.click();
	}

	public void clickDashboard() {
		webelement_dashboard.click();
	}

	public void clickworkSpace() {
		webelement_workSpace.click();
	}

	public void clickInvestigationConsole() {
		webelement_investigationConsole.click();
	}

	public void clicktransctionMonitoring() {
		webelement_transctionMonitoring.click();
	}

	public void clickleadGeneration() {
		webelement_leadGeneration.click();
	}

	public void clickLinkAnalysis() {
		webelement_linkAnalysis.click();
	}

	public void clickOrchestration() {
		webelement_orchestration.click();
	}

	public void clickAppManager() {
		webelement_appManager.click();
	}

	public void clickDataManagement() {
		webelement_dataManagement.click();
	}

	public void clickbigDataSciencePlatform() {
		webelement_bigDataSciencePlatform.click();
	}

	public void clickOnBoarding() {
		webelement_onboarding.click();
	}

	public void clickMarketIntelligence() {
		webelement_marketIntelligence.click();
	}

	public void clickEntity() {
		webelement_entity.click();
	}

	public void clickCases() {
		webelement_cases.click();
	}

	public void clickSystemMonitoring() {
		webelement_systemMonitoring.click();
	}

	public void clickAdverseTransaction() {
		webelement_adverseTransaction.click();
	}

	public void clickFraud() {
		webelement_fraud.click();
	}

	public void clickUnderwriting() {
		webelement_underwriting.click();
	}

	public void clickAuditTrail() {
		webelement_auditTrail.click();
	}

	public void clickSystemSettings() {
		webelement_systemSettings.click();
	}

	public void clickPolicyEnforcement() {
		webelement_policyEnforcement.click();
	}

	public void clickTransactionIntelligence() {
		webelement_transactionIntelligence.click();
	}

	public void clickeDiscover() {
		webelement_eDiscover.click();
	}

	public void clickQuestionaryBuilder() {
		webelement_questionaryBuilder.click();
	}

	public void clickDataCuration() {
		webelement_datacuration.click();
	}

	public void clickDecisionScoring() {
		webelement_decisionScoring.click();
	}

	public void clickdocumentParsing() {
		webelement_documentParsing.click();
	}

	public void clickPolicyEnforcementsourceManagement() {
		webelement_sourceManagement.click();
	}

}
