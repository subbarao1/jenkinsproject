package com.element.admin.integration.aml;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

public class AmlPage {
	public WebDriver driver;

	public int DRIVER_WAIT = 30; // 30 seconds

	/**
	 * Constructor.
	 *
	 * @param driver an instance of WebDriver
	 */
	public AmlPage(final WebDriver driver) {
		final ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, DRIVER_WAIT);
		PageFactory.initElements(finder, this);
		this.driver = driver;
	}

	@FindBy(xpath = "//div[4]/div/div[2]/div[2]/div[1]")
	public WebElement webelement_amllink;

	@FindBy(xpath = "//div[4]/div/div[2]/div[2]/div[3]")
	public WebElement webelement_socialListningLink;

	public void clickAML() {
		webelement_amllink.click();
	}

	public void clickSocialListning() {
		webelement_socialListningLink.click();
	}
}
