package com.element.admin.integration.act;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.element.admin.integration.aml.AmlPage;
import com.element.admin.integration.common.Login;

import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.openqa.selenium.WebDriver;

public class AuditTrailTest {
	WebDriver driver;
	String actual="", expected="";

	@BeforeClass
	public void startup() {
		final Login login=new Login(driver);
		driver = login.setup();
	}

	@Test(priority=1)
	public void domain() {
		final AmlPage aml=new AmlPage(driver);
		aml.clickAML();
	}
	@Test(priority=2)
	public void TS_003_01() throws InterruptedException {
		final AuditTrailPage audit=new AuditTrailPage(driver);
		audit.clickAudittrail();
		//System.out.println("tes :: "+audit.getLogActivityText());
		expected= "RECENT ACTIVITY";
		actual = audit.getLogActivityText();
		AssertJUnit.assertEquals(actual, expected);
	}
	@Test(priority=3)
	public void TS_003_02() throws InterruptedException {
		final AuditTrailPage audit=new AuditTrailPage(driver);
		audit.clicklogSearch();
		AssertJUnit.assertTrue(audit.recentActivityText("SEARCHED FOR AN ENTITY"));
	}
	@Test(priority=4)
	public void TS_003_03() throws InterruptedException {
		final AuditTrailPage audit=new AuditTrailPage(driver);
		audit.clickActivity();
		AssertJUnit.assertTrue(audit.recentActivityText("COMPLIANCE WIDGET REVIEW"));
	}
	@Test(priority=5)
	public void TS_003_04() throws InterruptedException {
		final AuditTrailPage audit=new AuditTrailPage(driver);
		audit.clickviewby();
	}
	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}
}
