package com.element.admin.integration.act;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import com.element.admin.integration.common.Util;

public class AuditTrailPage {
	public WebDriver driver;
	Util util; 

	public int DRIVER_WAIT = 30; // 30 seconds

	public AuditTrailPage(final WebDriver driver) {
		final ElementLocatorFactory finder = new AjaxElementLocatorFactory(
				driver, DRIVER_WAIT);
		PageFactory.initElements(finder, this);
		this.driver = driver;
		util=new Util();
	}

	@FindBy(xpath="//div[4]/div/div[2]/div[4]/div[2]/div/ul/li[5]/button/p")
	public WebElement webelement_auditTrail;

	@FindBy(xpath="//div[4]/div[2]/div/div/div/div[2]/div[3]/h4")
	public WebElement webelement_logActivity;
	@FindBy(xpath="//input[@id='textSearch']")
	public WebElement webelement_logSearchTxt;
	@FindBy(xpath="//div[@id='mCSB_13_container']/div[2]/form/div/button")
	public WebElement webelement_logSearchBtn;
	@FindBy(xpath="//select[@id='byActivityType']")
	public WebElement webelement_byActivity;
	@FindBy(xpath="//select[@id='byActivityType']/option[3]")
	public WebElement webelement_byActivityOption;
	@FindBy(xpath="//div[4]/div[2]/div/div/div/div[2]/div[2]/div/div/select")
	public WebElement webelement_viewBy;
	@FindBy(xpath="//div[4]/div[2]/div/div/div/div[2]/div[2]/div/div/select/option[1]")
	public WebElement webelement_viewByOptionMonth;

	public void clickAudittrail() {
		webelement_auditTrail.click();
	}
	public String getLogActivityText() throws InterruptedException {
		while (!isDisplayed(webelement_logActivity)) 
		{
			Thread.sleep(3000);
			System.out.println("Activity is not visible yet");
		}
		return webelement_logActivity.getText();
	}
	public void clicklogSearch() throws InterruptedException {
		webelement_logSearchTxt.sendKeys("SEARCHED FOR AN ENTITY");
		while (!isDisplayed(webelement_logSearchBtn)) 
		{
			Thread.sleep(3000);
			System.out.println("Activity is not visible yet");
		}
		webelement_logSearchBtn.click();
	}
	public void clickActivity() throws InterruptedException {
		webelement_byActivity.click();
		while (!isDisplayed(webelement_byActivityOption)) 
		{
			Thread.sleep(3000);
			System.out.println("Activity is not visible yet");
		}
		webelement_byActivityOption.click();
	}
	public void clickviewby() throws InterruptedException {
		webelement_viewBy.click();
		while (!isDisplayed(webelement_viewByOptionMonth)) 
		{
			Thread.sleep(3000);
			System.out.println("clickviewby is not visible yet");
		}
		webelement_viewByOptionMonth.click();
	}

	public boolean recentActivityText(String text) throws InterruptedException {

		Thread.sleep(6000);
		List<WebElement> ele =driver.findElements(By.xpath("//div/timeline-heading/div/h4"));
		int count=ele.size();
		boolean val = false;
		String name="",sname="";
		System.out.println("total "+count);
		for(int i=1;i<=ele.size();i++)
		{
			//Thread.sleep(3000);
			name=driver.findElement(By.xpath("(//div/timeline-heading/div/h4)["+i+"]")).getAttribute("innerText");
			//div[@id="mCSB_19_container"]/timeline/ul/timeline-event[89]/li/timeline-panel/div/div/p
			sname=driver.findElement(By.xpath("//div[@id='mCSB_15_container']/timeline/ul/timeline-event["+i+"]/li/timeline-panel/div/div/p")).getAttribute("innerText");
			if(name.contains(text))
			{
				System.out.println(""+sname);
				val=true;
			}
			else {
				System.out.println("no name  :"+name);
				val = false;
				break;
			}
		}
		
		
		return val;

	}

	public static boolean isDisplayed(WebElement element) {
		try {
			if(element.isDisplayed())
				return element.isDisplayed();
		}catch (NoSuchElementException ex) {
			return false;
		}
		return false;
	}
}
