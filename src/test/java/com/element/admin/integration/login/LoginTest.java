package com.element.admin.integration.login;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import com.element.admin.integration.aml.AmlPage;
import com.element.admin.integration.common.Login;
import com.element.admin.integration.common.Util;
import com.element.admin.integration.enrich.EnrichPage;
import com.element.admin.integration.shared.SharedPage;

public class LoginTest {
	// String companyname;
	// String jurisdiction;
	Util util = new Util();
	WebDriver driver;

	@BeforeClass
	public void startup() {
		final Login login = new Login(driver);
		driver = login.setup();
	}

	@Test(priority = 2)
	public void domain() {
		final AmlPage aml = new AmlPage(driver);
		aml.clickAML();
	}

	@Test(priority = 3)
	public void process() {
		final SharedPage spage = new SharedPage(driver);
		spage.clickAdvanceSearch();
	}

	@Test(priority = 4)
	public void searchCriteria() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickEntityType();
		enrich.clickEntityCompany();
		enrich.enterCompanyname(util.getCompanyName());
		enrich.enterjurisdiction(util.getJurisdiction());
		enrich.clickUpdate();
		enrich.clickSearch();
		enrich.clickFirstLink();
	}

	@Test(priority = 5)
	public void switchToNewWindow() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.switchingWindow(1);
		System.out.println("on new window");

		driver.findElement(By.xpath("//div[2]/div/div/div/ul/li[4]/a/i")).click();
		System.out.println("adv serach " + driver
				.findElement(By.xpath("//*[@id=\"entity-company-content\"]/div[1]/div/div/div/div[1]/div[2]/h2"))
				.getText());

	}

	@Test(priority = 6)
	public void dataNotFoundCount() throws InterruptedException {
		Thread.sleep(15000);

		final EnrichPage enrich = new EnrichPage(driver);

		enrich.getCompanyName();
		int i = enrich.checkDataNotFound("data");
		System.out.println("total data not founds are  : " + i);
		i = enrich.checkDataNotFound("loading..");
		System.out.println("total loadings are  : " + i);
	}

	/**
	 * driver close
	 */

	@AfterClass
	public void tearDown() {
		driver.close();
		driver.quit();
	}

}
