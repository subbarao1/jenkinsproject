package com.element.admin.integration.enrich;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.openqa.selenium.WebDriver;

import com.element.admin.integration.enrich.EnrichPage;
import com.element.admin.integration.aml.AmlPage;
import com.element.admin.integration.common.Login;
import com.element.admin.integration.common.Util;
import com.element.admin.integration.shared.SharedPage;

public class ComplianceTest {
	String companyname;
	String jurisdiction;
	String actual = "", expected = "";
	Util util = new Util();

	WebDriver driver;

	@BeforeClass
	public void startup() {
		final Login login = new Login(driver);
		driver = login.setup();
	}

	@Test(priority = 2)
	public void domain() {
		final AmlPage aml = new AmlPage(driver);
		aml.clickAML();
	}

	@Test(priority = 3)
	public void process() {
		final SharedPage spage = new SharedPage(driver);
		spage.clickAdvanceSearch();
	}

	/*
	 * @Test(enabled=false,priority=4) public void clickSavedSearch() throws
	 * InterruptedException { final EnrichPage enrich= new EnrichPage(driver);
	 * enrich.clickSavedSearch(); Thread.sleep(3000);
	 * System.out.println("save search reuslts are : "+enrich.getSearchResultText())
	 * ; }
	 */
	@Test(priority = 4)
	public void TS_004_08() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickEntityType();
		enrich.clickEntityCompany();
		enrich.enterCompanyname(util.getCompanyName());
		enrich.enterjurisdiction(util.getJurisdiction());
		enrich.clickUpdate();
		enrich.clickSearch();
		// clickFirstResult();
	}

	@Test(priority = 5)
	public void clickFirstResult() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickFirstLink();
	}

	@Test(priority = 6)
	public void switchToaNewWindow() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.switchingWindow(1);
		System.out.println("on new window");
		enrich.clickCompliance();
		// System.out.println("adv serach " +
		// driver.findElement(By.xpath("//*[@id=\"entity-company-content\"]/div[1]/div/div/div/div[1]/div[2]/h2")).getText());
		System.out.println("----");
	}

	@Test(priority = 7)
	public void TS_004_20() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		Thread.sleep(3000);
		enrich.clickCheckList();
		enrich.switchingWindow(2);
		// enrich.clickNextButton(14);

		enrich.legalExistence();

		enrich.customerProductInformation();
		enrich.customerActivityHighRisk();
		enrich.ownershipStructure();
		enrich.listingRegulationStatus();
		enrich.complexOwnershipStructure();
		enrich.UBO();
		enrich.mainPrincipals();
		enrich.associatedParties();
		enrich.Principals();
		enrich.sourceofFunds();
		enrich.Screening();
		enrich.INITIAL_RISK_RATING();
		enrich.UHRC();
		enrich.profileSummary();

		// enrich.clickSubmitButton();
		System.out.println("+++++");
		actual = "Thank you!";
		expected = enrich.getThankyouMassege();
		AssertJUnit.assertEquals(actual, expected);
		// driver.close();
		System.out.println("--7--");
	}

	@Test(enabled = false, priority = 8)
	public void TS_004_21() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.switchingWindow(1);
		enrich.clickGenerateReport();

	}

	@Test(priority = 9)
	public void TS_004_22() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.switchingWindow(1);
		enrich.clickOnBoarding();
		Thread.sleep(5000);
		System.out.println("->" + enrich.getOnBoardingTesxt());
		enrich.clickonboardingclose();
		System.out.println("--9--");
	}

	@Test(priority = 10)
	public void TS_004_24() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		// Thread.sleep(5000);
		enrich.switchingWindow(1);
		enrich.screenResultLoading();
		System.out.println("ready");
		enrich.clickScreeningResult();
		enrich.typeFirstName();
		enrich.typeLastName();
		enrich.typeCompanyName();
		enrich.typeDateOfBirth();
		enrich.clickSRSave();
		System.out.println("///" + enrich.getTextAddedScreenResult());
		System.out.println("--10--");
	}

	@Test(priority = 11)
	public void TS_004_25() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		// enrich.switchingWindow(1);
		enrich.screenResultLoading();
		Thread.sleep(5000);
		System.out.println("ready1");
		enrich.clickExpand();
		actual = "Ownership Structure";
		expected = enrich.getExpandModelText();
		enrich.clickExpandClose();
		AssertJUnit.assertEquals(actual, expected);
		// System.out.println("modal text"+enrich.getExpandModelText());
		System.out.println("--11--");
	}

	@Test(priority = 12)
	public void TS_004_23() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.screenResultLoading();
		enrich.clickownershipControlpercentage();
		enrich.clickOwnershipConfigurationApply();
		enrich.screenResultLoading();
		enrich.clickpepAlert();
		enrich.clickSanctionsConfidenceSlider();
		enrich.clicknewsPeriodSlider();
		enrich.clickScreenConfigurationApply();
		System.out.println("--12--");
	}

	@Test(priority = 13)
	public void TS_004_27() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickScreenresultCheckBox();
		enrich.clickActivateScreen();
		enrich.waitActivteScreenSpinner();
		enrich.clickFinanceCrime();
		enrich.waitAdverceNewsModel();
		System.out.println("everything is done");
		Thread.sleep(5000);
	}

	@Test(dependsOnMethods = "TS_004_27", priority = 14)
	public void TS_004_28() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		System.out.println("peps found : " + enrich.getAdverseNewsModelText(1));
	}

	@Test(dependsOnMethods = "TS_004_27", priority = 17)
	public void TS_004_29() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		System.out.println("financial crime found : " + enrich.getAdverseNewsModelText(4));
	}

	@Test(dependsOnMethods = "TS_004_27", priority = 15)
	public void TS_004_30() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		System.out.println("sanctions found : " + enrich.getAdverseNewsModelText(2));
	}

	@Test(dependsOnMethods = "TS_004_27", priority = 16)
	public void TS_004_31() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		System.out.println("jurisdictions found : " + enrich.getAdverseNewsModelText(3));
	}

	@Test(dependsOnMethods = "TS_004_27", priority = 18)
	public void TS_004_27_1() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		System.out.println("News found : " + enrich.getAdverseNewsModelText(5));
		enrich.clickANMClose();
	}

	@Test(priority = 19)
	public void TS_004_26() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickOSCompanyname();
		enrich.clickOSAddPerson();
		enrich.typeOSFirstName();
		enrich.clickOSAdd();
	}

	@Test(priority = 22)
	public void TS_004_32() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		// driver.close();
		enrich.switchingWindow(0);
		enrich.clickSearchCancel();
		enrich.clickEntityType();
		enrich.clickEntityCompany();
		enrich.enterCompanyname("VOLKSWAGEN");
		enrich.enterjurisdiction("Germany");
		enrich.clickUpdate();
		enrich.clickSearch();
		enrich.clickFirstLink();

		enrich.switchingWindow(3);
		enrich.screenResultLoading();
		// enrich.ownershipstructureLoading();
		expected = "VOLKSWAGEN AG VZ.";
		actual = enrich.getHeaderText();
		AssertJUnit.assertEquals(actual, expected);
		System.out.println("--12--");

	}

	@Test(priority = 23)
	public void TS_004_33() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		// driver.close();
		enrich.switchingWindow(0);
		enrich.clickSearchCancel();
		enrich.clickEntityType();
		enrich.clickEntityCompany();
		enrich.enterCompanyname("Renault");
		enrich.enterjurisdiction("France");
		enrich.clickUpdate();
		enrich.clickSearch();

		enrich.clickFirstLink();

		enrich.switchingWindow(4);
		enrich.screenResultLoading();
		// enrich.ownershipstructureLoading();
		expected = "RENAULT";
		actual = enrich.getHeaderText();
		AssertJUnit.assertEquals(actual, expected);
		System.out.println("--13--");

	}

	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}
}
