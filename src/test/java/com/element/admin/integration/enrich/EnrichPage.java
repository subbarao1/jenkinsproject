package com.element.admin.integration.enrich;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import com.element.admin.integration.common.Util;

public class EnrichPage {
	public WebDriver driver;
	Util util;
	int count = 0;

	public int DRIVER_WAIT = 30; // 30 seconds

	/**
	 * Constructor.
	 *
	 * @param driver an instance of WebDriver
	 */
	public EnrichPage(final WebDriver driver) {
		final ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, DRIVER_WAIT);
		PageFactory.initElements(finder, this);
		this.driver = driver;
		util = new Util();
	}

	@FindBy(xpath = "//button[@id='single-button']")
	public WebElement webelement_entityType;

	@FindBy(xpath = "//*[@id='headingcompany']/div/div[2]/div/ul/li[1]/a")
	public WebElement webelement_entityCompany;

	@FindBy(xpath = "//input[@id='companyname']")
	public WebElement webelement_companyName;

	@FindBy(xpath = "//input[@id='input']")
	public WebElement webelement_jurisdiction;

	@FindBy(xpath = "//div[@id='enrich-dashboard']/div[6]/div/div/div[3]/button[1]")
	public WebElement webelement_update;

	@FindBy(xpath = "//*[@id='accordion']/div/div[1]/button")
	public WebElement webelement_search;

	@FindBy(xpath = "//div[4]/div[2]/div[2]/div/div[2]/ul/li/div[1]/div/div[2]/div[2]/div[2]/div/div[1]/div[1]/div/div[2]")
	public WebElement webelement_firstLink;

	@FindBy(xpath = "//li[contains(.,'Financial')]")
	public WebElement webelement_financial;

	@FindBy(xpath = "//*[text()='DATA NOT FOUND']")
	public WebElement webelement_dataNotFound;

	@FindBy(xpath = "//div[2]/div/div[2]/div/div/div/div/div[3]/div/div/div") // div[@id='mCSB_42_container']//div[@class='alert-message
																				// loading-message
																				// ng-scope'][contains(text(),'Loading...')]
	public WebElement webelement_stockHistory;

	@FindBy(xpath = "//div[2]/div[3]/div/div/p") // *[@id="mCSB_47_container"]/p //*[@id="mCSB_48_container"]/p
	public WebElement webelement_balanceSheet;

	@FindBy(xpath = "//div/div[3]/div/div/div/div/div[2]/div/div/p") // *[@id="mCSB_45_container"]/p
	public WebElement webelement_cashFlow;

	@FindBy(xpath = "//div[3]/div[3]/div/div/p") // *[@id="mCSB_49_container"]/p
	public WebElement webelement_incomeStmt;

	@FindBy(xpath = "//div[5]/div/div[4]/div[2]/div") // *[@id="mCSB_13_container"]/div[5]/div/div[4]/div[2]/div
	public WebElement webelement_shareHolderType;

	@FindBy(xpath = "//div[5]/div/div[3]/div[2]/div") // *[@id="mCSB_13_container"]/div[5]/div/div[3]/div[2]/div
	public WebElement webelement_totalRevenue;

	@FindBy(xpath = "//div[4]/div/div/div[4]/div/div/div/div[2]/div/div/div") // *[@id="mCSB_50_container"]/div
	public WebElement webelement_topHolders;

	@FindBy(xpath = "//div[4]/div/div/div[4]/div/div/div[2]/div/div/div[2]/div/div/div") // *[@id="mCSB_51_container"]/div
	public WebElement webelement_shareHolderNetwork;

	@FindBy(xpath = "//*[@id='entity-company-content']/div[1]/div[1]/div/div/div[1]/div[2]/h2")
	public WebElement webelement_cNameText;

	@FindBy(xpath = "//div[4]/div/div[2]/div[1]/div[2]/div/ul/li[1]/button")
	public WebElement webelement_advanceSearch;

	@FindBy(xpath = "//div[4]/div/div[2]/div[2]/div[3]/div")
	public WebElement webelement_socialListningLink;

	@FindBy(xpath = "//div[@id='fixed']/p")
	public WebElement webelement_savedSearch;
	@FindBy(xpath = "//div[@id='enrich-advanced-search']/div[2]/ul/li/div/div/div[2]/div[2]/div")
	public WebElement webelement_searchResultHeader;
	@FindBy(xpath = "//div[@id='mCSB_14_container']/div/p")
	public WebElement webelement_recentSearch;
	@FindBy(xpath = "//div[@id='criteria-searches']/div[2]/button[2]")
	public WebElement webelement_searchSave;
	@FindBy(xpath = "//span/div")
	public WebElement webelement_saveSuccessMessage;
	@FindBy(xpath = "//div[@id='criteria-searches']/div[2]/button[3]")
	public WebElement webelement_serachCancel;

	// div[4]/div[1]/div[3]/div/div[2]/div[4]/a/span
	// div[4]/div[1]/div[3]/div/div[2]/div[2]/a/span
	@FindBy(xpath = "//div[4]/div[1]/div[3]/div/div[2]/div[2]/a/span")
	public WebElement webelement_CheckList;
	@FindBy(xpath = "//a[@id='generateReport']/span")
	public WebElement webelement_generateReport;
	@FindBy(xpath = "//div[4]/div[1]/div[3]/div/div[2]/div[3]/a/span")
	public WebElement webelement_onBoarding;
	@FindBy(xpath = "//li[contains(.,'Overview')]")
	public WebElement webelement_overview;
	@FindBy(xpath = "//*[@id='entity-company-content']/div[1]/div/div/div/div[2]/div/div[1]/div/ul/li[2]/a/i")
	public WebElement webelement_Compliance;
	@FindBy(xpath = "//li[contains(.,'Leadership')]")
	public WebElement webelement_Leadership;
	@FindBy(xpath = "//li[contains(.,'Risk Alerts')]")
	public WebElement webelement_RiskAlerts;
	@FindBy(xpath = "//div[2]/div/div/div/ul/li[6]/a/i")
	public WebElement webelement_ThreatsIntelligence;
	@FindBy(xpath = "//div[2]/div/div/div/ul/li[8]/a/i")
	public WebElement webelement_SocialMedia;
	@FindBy(xpath = "//div[2]/div/div/div/ul/li[9]/a/i")
	public WebElement webelement_Media;
	@FindBy(xpath = "//li[contains(.,'Latest News')]")
	public WebElement webelement_LatestNews;
	@FindBy(xpath = "//div[@id='mCSB_13']")
	public WebElement webelement_recentdiv;
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[1]/div/ul/li")
	public WebElement webelement_totalentiryTabs;

	// overview text elements
	@FindBy(xpath = "//h3[contains(.,'Who Are We')]")
	public WebElement webelement_WhoAreWe;
	@FindBy(xpath = "//h3[contains(.,'Leadership')]")
	public WebElement webelement_LeadershipText;
	@FindBy(xpath = "//h3[contains(.,'Stock Performance')]")
	public WebElement webelement_StockPerformance;
	@FindBy(xpath = "//h3[contains(.,'Visual Link Analysis')]")
	public WebElement webelement_VisualLinkAnalysis;
	@FindBy(xpath = "//h3[contains(.,'Associated Companies')]")
	public WebElement webelement_AssociatedCompanies;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div/div/div/h3")
	public WebElement webelement_ContactAddress;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div[1]/div/div[2]/h3")
	public WebElement webelement_RiskScore;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div/div/div[5]/h3")
	public WebElement webelement_RiskRating;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div/div/div[6]/h3")
	public WebElement webelement_RelatedEntities;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div/div/div[7]/h3")
	public WebElement webelement_AssociatedLocations;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div/div/div[8]/h3")
	public WebElement webelement_O_LatestNews;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div/div/div[9]/h3")
	public WebElement webelement_AssociatedIndustries;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div/div/div[10]/h3")
	public WebElement webelement_DigitalFootPrint;

	// compliance text elements
	@FindBy(xpath = "//h4[contains(.,'Company Information')]")
	public WebElement webelement_CompanyInformation;
	@FindBy(xpath = "//h3[contains(.,'Company details')]")
	public WebElement webelement_CompanyDetails;
	@FindBy(xpath = "//h3[contains(.,'Company Identifiers')]")
	public WebElement webelement_CompanyIdentifiers;
	@FindBy(xpath = "//h4[contains(.,'Ownership Configurations:')]")
	public WebElement webelement_OwnershipConfigurations;
	@FindBy(xpath = "//span[contains(.,'ownership structure')]")
	public WebElement webelement_OwnershipStructure;
	@FindBy(xpath = "//h4[contains(.,'Screening Configurations:')]")
	public WebElement webelement_ScreeningConfigurations;
	@FindBy(xpath = "//h4[contains(.,'screening result')]")
	public WebElement webelement_ScreeningResult;
	@FindBy(xpath = "//h3[contains(.,'countries of operation')]")
	public WebElement webelement_CountriesOfOperation;
	@FindBy(xpath = "//h4[contains(.,'associated documents')]")
	public WebElement webelement_AssociatedDocuments;

	// Leadership Components
	@FindBy(xpath = "//h3[contains(.,'Key Executive')]")
	public WebElement webelement_KeyExecutive;

	@FindBy(xpath = "//h3[contains(.,'Board of Directors')]")
	public WebElement webelement_BoardofDirectors;
	@FindBy(xpath = "//h3[contains(.,'Committee')]")
	public WebElement webelement_Committee;
	@FindBy(xpath = "//h3[contains(.,'Rating Trend')]")
	public WebElement webelement_RatingTrend;
	@FindBy(xpath = "//h3[contains(.,'TOP INFLUENCERS')]")
	public WebElement webelement_TOPINFLUENCERS;
	@FindBy(xpath = "//h3[contains(.,'People Also Visited')]")
	public WebElement webelement_PeopleAlsoVisited;
	@FindBy(xpath = "//h3[contains(.,'Near By Companies')]")
	public WebElement webelement_NearByCompanies;
	@FindBy(xpath = "//h3[contains(.,'Company Members')]")
	public WebElement webelement_CompanyMembers;
	@FindBy(xpath = "//h3[contains(.,'Positive Business Outlook')]")
	public WebElement webelement_PositiveBusinessOutlook;
	@FindBy(xpath = "//h3[contains(.,'Associated Persons')]")
	public WebElement webelement_AssociatedPersons;
	@FindBy(xpath = "//h3[contains(.,'Leadership Clusters')]")
	public WebElement webelement_LeadershipClusters;
	@FindBy(xpath = "//h3[contains(.,'Career Opportunities ')]")
	public WebElement webelement_CareerOpportunities;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div[4]/div/div[1]/h3")
	public WebElement webelement_Leadership_RiskScore;

	// Financial Components
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div[5]/div/div[1]/h3")
	public WebElement webelement_f_RiskScore;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div[5]/div/div[3]/h3")
	public WebElement webelement_f_TotalRevenuesVsYear;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div[5]/div/div[4]/h3")
	public WebElement webelement_f_ShareholdersType;
	@FindBy(xpath = "//h3[contains(.,'Net Cash')]")
	public WebElement webelement_f_NetCashFlow;
	@FindBy(xpath = "//h3[contains(.,'Balance Sheet')]")
	public WebElement webelement_f_BalanceSheet;
	@FindBy(xpath = "//h3[contains(.,'Income Statement')]")
	public WebElement webelement_f_IncomeStatement;
	@FindBy(xpath = "//h3[contains(.,'Top Holders')]")
	public WebElement webelement_f_TopHolders;
	@FindBy(xpath = "//h3[contains(.,'Share Holder Network')]")
	public WebElement webelement_f_ShareHolderNetwork;// h3[contains(.,'Stock History')]
	@FindBy(xpath = "//h3[contains(.,'Stock History')]")
	public WebElement webelement_f_StockHistory;

	// Risk Alerts Components
	@FindBy(xpath = "//h3[contains(.,'Alert Timeline')]")
	public WebElement webelement_AlertTimeline;
	@FindBy(xpath = "//h3[contains(.,'Political Donations Trend')]")
	public WebElement webelement_PoliticalDonationsTrend;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div[6]/div/div[1]/h3")
	public WebElement webelement_R_RiskScore;
	@FindBy(xpath = "//h3[contains(.,'Political Ratio')]")
	public WebElement webelement_PoliticalRatio;
	@FindBy(xpath = "//h3[contains(.,'Risk Ratio')]")
	public WebElement webelement_RiskRatio;
	@FindBy(xpath = "//h3[contains(.,'Interlocks')]")
	public WebElement webelement_Interlocks;
	@FindBy(xpath = "//h3[contains(.,'Pep Alerts')]")
	public WebElement webelement_PepAlerts;
	@FindBy(xpath = "//h3[contains(.,'Fraud Alerts')]")
	public WebElement webelement_FraudAlerts;
	@FindBy(xpath = "//h3[contains(.,'Cyber Alerts')]")
	public WebElement webelement_CyberAlerts;
	@FindBy(xpath = "//h3[contains(.,'Sanctioned List')]")
	public WebElement webelement_SanctionedList;
	// @FindBy(xpath="//*[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[5]/div/div/div[4]/div/div/div[2]/div/h3")
	// *[@id="entity-company-content"]/div[1]/div[1]/div/div/div[2]/div/div[2]/div[5]/div/div/div[4]/div/div/div[2]/div/h3
	// @FindBy(xpath="//*[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[6]/div/div/div[4]/div/div/div[2]/div/h3")
	@FindBy(xpath = "//h3[contains(.,'News')]")
	public WebElement webelement_News;
	@FindBy(xpath = "//h3[contains(.,'Pep Relationship Chart')]")
	public WebElement webelement_PepRelationshipChart;
	@FindBy(xpath = "//h3[contains(.,'Black List')]")
	public WebElement webelement_SuppliersBlackList;
	@FindBy(xpath = "//h3[contains(.,'Industrial Security Incidents')]")
	public WebElement webelement_IndustrialSecurityIncidents;
	@FindBy(xpath = "//h3[contains(.,'Criminal Records')]")
	public WebElement webelement_CriminalRecords;
	@FindBy(xpath = "(//h3[contains(.,'Dark Web')])[2]")
	public WebElement webelement_DarkWeb;

	// Latest News Components
	@FindBy(xpath = "//h3[contains(.,'Overall Sentiment')]")
	public WebElement webelement_OverallSentiment;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div[7]/div/div[1]/h3")
	public WebElement webelement_News_RiskScore;
	@FindBy(xpath = "//h3[contains(.,'General Sentiments')]")
	public WebElement webelement_GeneralSentiments;
	@FindBy(xpath = "//h3[contains(.,'Latest News')]")
	public WebElement webelement_News_LatestNews;
	@FindBy(xpath = "//h3[contains(.,'Press Releases')]")
	public WebElement webelement_PressReleases;
	@FindBy(xpath = "//h3[contains(.,'Popular Tags')]")
	public WebElement webelement_PopularTags;
	@FindBy(xpath = "//h3[contains(.,'Recent Events')]")
	public WebElement webelement_RecentEvents;
	@FindBy(xpath = "//h3[contains(.,'Relationship Charts By News')]")
	public WebElement webelement_RelationshipChartsByNews;
	@FindBy(xpath = "//h3[contains(.,'News Locations')]")
	public WebElement webelement_NewsLocations;
	@FindBy(xpath = "//label[contains(.,'No')]")
	public WebElement webelement_noAnswer1;
	@FindBy(xpath = "(//label[contains(.,'No')])[2]")
	public WebElement webelement_noAnswer2;
	@FindBy(xpath = "(//label[contains(.,'No')])[3]")
	public WebElement webelement_noAnswer3;

	// threats Intelligence
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[3]/div/div[1]/div[1]/div[1]/div[1]/h3")
	public WebElement webelement_ti_peerGroup;
	@FindBy(xpath = "//*[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[3]/div/div[1]/div[2]/div[1]/table/caption")
	public WebElement webelement_ti_similarcompanies;
	@FindBy(xpath = "//*[@id='mCSB_13_container']/div[3]/div/div[1]/h3")
	public WebElement webelement_ti_riskscore;
	@FindBy(xpath = "//*[@id='mCSB_13_container']/div[3]/div/div[2]/div/div[1]/h3")
	public WebElement webelement_ti_bytopcountries;
	@FindBy(xpath = "//*[@id='mCSB_13_container']/div[3]/div/div[3]/div/div[1]/h3")
	public WebElement webelement_ti_bytoptechnologies;
	@FindBy(xpath = "//*[@id='mCSB_13_container']/div[3]/div/div[4]/div/ul/li/div/div[1]/h4")
	public WebElement webelement_ti_cybersecurity;
	@FindBy(xpath = "//*[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[3]/div/div[2]/div[2]/div[1]/div[1]/div/div[1]/h3")
	public WebElement webelement_ti_selectlocation;
	@FindBy(xpath = "//*[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/div[1]/h3")
	public WebElement webelement_ti_selectindustry;
	@FindBy(xpath = "//*[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[3]/div/div[2]/div[1]/div/div[1]/div/h3")
	public WebElement webelement_ti_displaying;
	@FindBy(xpath = "//*[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[3]/div/div[2]/div[2]/div[1]/div[3]/div/div[1]/h3")
	public WebElement webelement_ti_tagcloud;
	@FindBy(xpath = "//*[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[3]/div/div[2]/div[2]/div[2]/div[1]/div/div[1]/h3")
	public WebElement webelement_ti_threattimeline;
	@FindBy(xpath = "//*[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/div[1]/h3")
	public WebElement webelement_ti_IncidentsClassificationByIndustry;
	@FindBy(xpath = "//*[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[3]/div/div[2]/div[2]/div[2]/div[3]/div/div[1]/h3")
	public WebElement webelement_ti_incidentsclassification;
	@FindBy(xpath = "//*[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[3]/div/div[2]/div[2]/div[2]/div[4]/div/div[1]/h3")
	public WebElement webelement_ti_relationships;

	// Social Media
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[8]/div/div/div[2]/h3")
	public WebElement webelement_sm_ActivityFeed;
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[8]/div/div/div[3]/div/div/div[1]/div/h3")
	public WebElement webelement_sm_RecentTweets;
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[8]/div/div/div[3]/div/div/div[2]/div/h3")
	public WebElement webelement_sm_InstagramPosts;
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[8]/div/div/div[3]/div/div/div[3]/div/h3")
	public WebElement webelement_sm_FacebookSocialFeeds;
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[8]/div/div/div[4]/div/div/div[1]/div/h3")
	public WebElement webelement_sm_RecentLinkedinPost;
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[8]/div/div/div[4]/div/div/div[2]/div/h3")
	public WebElement webelement_sm_GooglePlusPost;
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[8]/div/div/div[4]/div/div/div[3]/div[1]/h3")
	public WebElement webelement_sm_LiveFeed;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div[8]/div/div[1]/h3")
	public WebElement webelement_sm_RISKSCORE;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div[8]/div/div[3]/h3")
	public WebElement webelement_sm_ACTIVITYLOCATIONS;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div[8]/div/div[4]/h3")
	public WebElement webelement_sm_TWITTERTAGWORDS;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div[8]/div/div[5]/h3")
	public WebElement webelement_sm_INTERACTIONRATIO;

	// Media
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[2]/div[9]/div/h3[3]")
	public WebElement webelement_m_images;
	@FindBy(xpath = "//div[@id='mCSB_13_container']/div[9]/div/div[1]/h3")
	public WebElement webelement_m_RiskScore;

	@FindBy(xpath = "//div[10]/div[3]/div/p/textarea")
	public WebElement webelement_ta_lr;
	@FindBy(xpath = "//div[3]/div/div/div/div")
	public WebElement webelement_rbNo1;
	@FindBy(xpath = "//div[3]/div/div/div/div/div/div")
	public WebElement webelement_rbNo2;
	@FindBy(xpath = "//div[5]/div[3]/div/p/textarea")
	public WebElement webelement_ta_os;
	@FindBy(xpath = "//div[3]/div/div/div[2]/div/div/div")
	public WebElement webelement_rbNo3;
	@FindBy(xpath = "//div[3]/div/div/div[2]/div")
	public WebElement webelement_rbNo4;
	@FindBy(xpath = "//div[3]/div/div/div[2]/div")
	public WebElement webelement_rbNo5;
	@FindBy(xpath = "//div[3]/div/div/div[2]/div")
	public WebElement webelement_rbNo6;
	@FindBy(xpath = "//div[3]/div/div/div[2]/div")
	public WebElement webelement_rbNo7;
	@FindBy(xpath = "//div[3]/div/div/div[2]/div/div/div")
	public WebElement webelement_rbNo8;
	@FindBy(xpath = "//div[3]/div/div/div/div/div/div")
	public WebElement webelement_rbNo9;
	@FindBy(xpath = "//div[3]/div[3]/div/div/div/div/div/div")
	public WebElement webelement_rbNo10;
	@FindBy(xpath = "//div[4]/div[3]/div/div/div[2]/div/div/div")
	public WebElement webelement_rbNo11;
	@FindBy(xpath = "//div[5]/div[3]/div/div/div[2]/div/div/div")
	public WebElement webelement_rbNo12;
	@FindBy(xpath = "//div[8]/div[3]/div/div/div[3]/div/div/div")
	public WebElement webelement_rbNo13;
	@FindBy(xpath = "//input[@id='answer612X235X5565s']")
	public WebElement webelement_rbNo;

	@FindBy(xpath = "//button[contains(.,'Next')]")
	public WebElement webelement_NextButton;
	@FindBy(xpath = "//div[4]/div[3]/div/p/textarea")
	public WebElement webelement_textArea1;
	@FindBy(xpath = "//div[4]/div[3]/div/p/textarea")
	public WebElement webelement_textArea2;
	@FindBy(xpath = "//textarea")
	public WebElement webelement_textArea3;
	@FindBy(xpath = "//div[4]/div[3]/div/p/textarea")
	public WebElement webelement_textArea4;
	@FindBy(xpath = "//div/textarea")
	public WebElement webelement_textArea5;
	@FindBy(xpath = "//div[2]/div/div/textarea")
	public WebElement webelement_textArea6;
	@FindBy(xpath = "//div[3]/div/div/textarea")
	public WebElement webelement_textArea7;
	@FindBy(xpath = "//div[3]/div/div/textarea")
	public WebElement webelement_textArea8;
	@FindBy(xpath = "//div[5]/div/div/textarea")
	public WebElement webelement_textArea9;
	@FindBy(xpath = "//div[6]/div/div/textarea")
	public WebElement webelement_textArea10;
	@FindBy(xpath = "//div[7]/div/div/textarea")
	public WebElement webelement_textArea11;
	@FindBy(xpath = "//div[8]/div/div/textarea")
	public WebElement webelement_textArea12;
	@FindBy(xpath = "//div[9]/div/div/textarea")
	public WebElement webelement_textArea13;
	@FindBy(xpath = "//div[6]/div[3]/div/div/div/div/div/textarea")
	public WebElement webelement_textArea14;
	@FindBy(xpath = "//div[6]/div[3]/div/div/div[2]/div/div/textarea")
	public WebElement webelement_textArea15;
	@FindBy(xpath = "//div[6]/div[3]/div/div/div[3]/div/div/textarea")
	public WebElement webelement_textArea16;
	@FindBy(xpath = "//div[6]/div[3]/div/div/div[4]/div/div/textarea")
	public WebElement webelement_textArea17;
	@FindBy(xpath = "//div[6]/div[3]/div/div/div[5]/div/div/textarea")
	public WebElement webelement_textArea18;
	@FindBy(xpath = "//div[6]/div[3]/div/div/div[6]/div/div/textarea")
	public WebElement webelement_textArea19;
	@FindBy(xpath = "//div[6]/div[3]/div/div/div[7]/div/div/textarea")
	public WebElement webelement_textArea20;
	@FindBy(xpath = "//div[6]/div[3]/div/div/div[8]/div/div/textarea")
	public WebElement webelement_textArea21;

	@FindBy(xpath = "//div[8]/div[3]/div/div/div/div/div/textarea")
	public WebElement webelement_textArea22;
	@FindBy(xpath = "//div[8]/div[3]/div/div/div[2]/div/div/textarea")
	public WebElement webelement_textArea23;
	@FindBy(xpath = "//div[9]/div[3]/div/p/textarea")
	public WebElement webelement_textArea24;
	@FindBy(xpath = "//div[10]/div[3]/div/p/textarea")
	public WebElement webelement_textArea25;
	@FindBy(xpath = "//div[11]/div[3]/div/p/textarea")
	public WebElement webelement_textArea26;
	@FindBy(xpath = "//div[12]/div[3]/div/p/textarea")
	public WebElement webelement_textArea27;

	@FindBy(xpath = "//button[@id='movesubmitbtn']")
	public WebElement webelement_SubmitButton;

	@FindBy(xpath = "//*[@id='main-col']/table/tbody/tr/td/span") // *[@id="main-col"]/table/tbody/tr/td/span
	public WebElement webelement_ThankYouMessage;
	@FindBy(xpath = "//h2[contains(.,'Table of Contents')]")
	public WebElement webelement_ReportTableOfContent;
	@FindBy(xpath = "//span")
	public WebElement webelement_spinner;
	@FindBy(xpath = "//div[@class='modal-dialog ']/div/div/div[1]/button")
	public WebElement webelement_OnboardingClose;
	@FindBy(xpath = "//div[@class='modal-dialog ']/div/div/div[2]/h3") // html/body/div[1]/div/div/div/div[2]/h3
	public WebElement webelement_OnboardingText;

	// Screening Results
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div/div[4]/div[4]/div/div/div[2]/span[1]")
	public WebElement webelement_ScreeningResultLoading;
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div/div[4]/div[4]/div/div/div[1]/h4/span/button")
	public WebElement webelement_ScreeningResultAdd;
	@FindBy(xpath = "//div[@class='modal-content']/div[1]/div[2]/div/div/div[1]/div/form/div/input")
	public WebElement webelement_sr_firstName;
	@FindBy(xpath = "(//div[@class='modal-content']/div[1]/div[2]/div/div/div[1]/div/form/div/input)[2]")
	public WebElement webelement_sr_lastName;
	@FindBy(xpath = "(//input[@id='input'])[1]")
	public WebElement webelement_sr_companyName;
	@FindBy(xpath = "(//div[@class='modal-content']/div[1]/div[2]/div/div/div[1]/div/form/div/input)[3]")
	public WebElement webelement_sr_dob;
	@FindBy(xpath = "//div[1]/div/div/div/div[2]/div/div/div/div[1]/form/div[6]/select")
	public WebElement webelement_sr_role;
	@FindBy(xpath = "//div[1]/div/div/div/div[2]/div/div/div/div[1]/form/div[5]/div/div/button/span[1]")
	public WebElement webelement_sr_classification;
	@FindBy(xpath = "//div[1]/div/div/div/div[2]/div/div/div/div[1]/form/button[2]")
	public WebElement webelement_sr_save;
	@FindBy(xpath = "//div[@id='flowChartViewDiv1']/div[2]/h4/ul[1]/li/span")
	public WebElement webelement_os_text;
	// @FindBy(xpath="//div[@id='flowChartViewDiv1']/div[2]/ul/li[2]/a")
	@FindBy(css = ".t-0")
	public WebElement webelement_os_expand;

	@FindBy(xpath = "(//td/span[contains(.,'fName lName')])[1]")
	public WebElement webelement_sr_addedResult;
	@FindBy(xpath = "//div[@id='mCSB_97_container']/tr[1]/td[2]")
	public WebElement webelement_sr_addedResultClick;
	@FindBy(xpath = "//div[@id='maximizeAllchart']/div[1]/p")
	public WebElement webelement_os_expandModalText;
	@FindBy(xpath = "//div[@id='maximizeAllchart']/div[1]/button/span")
	public WebElement webelement_os_expandClose;
	@FindBy(xpath = "//div[@id='flowChartViewDiv1']/div[2]/ul/li[1]/div")
	public WebElement webelement_os_loading;
	@FindBy(xpath = "//div[@id='mCSB_95_container']/tr[2]/td[13]/button[2]/i")
	public WebElement webelement_sr_delete;
	@FindBy(xpath = "//div[1]/div/div/div[2]/button[1]")
	public WebElement webelement_sr_deleteCancel;
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div/div/div/div[1]/div[2]/h2")
	public WebElement webelement_headerText;

	@FindBy(xpath = "//div[@id='sliderAvgExpenCompliance']")
	public WebElement webelement_ownershipControlpercentage;
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div/div[3]/div/div/div/div/div[2]/div/div/div/div[2]/button")
	public WebElement webelement_ownershipConfigurationApply;
	@FindBy(xpath = "//div[@id='pepliderCompliance']")
	public WebElement webelement_pepslider;
	@FindBy(xpath = "//div[@id='sanctionsliderCompliance']")
	public WebElement webelement_sanctionsConfidentslider;
	@FindBy(xpath = "//div[@id='yearsliderCompliance']")
	public WebElement webelement_newsperiodSlider;
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div/div[4]/div[3]/div/div/div/div/div[2]/div/div/div/div[2]/button")
	public WebElement webelement_screeningConfigurationApply;
	@FindBy(xpath = "//div/tr[1]/td[1]/div/input")
	public WebElement webelement_screenresultCheckbox;
	@FindBy(xpath = "//div/tr[1]/td[5]/span")
	public WebElement webelement_screenresultResidance;
	@FindBy(xpath = "//div/tr[1]/td[6]")
	public WebElement webelement_screenresultDOB;
	@FindBy(xpath = "//div/tr[1]/td[3]")
	public WebElement webelement_screenresultCompany;
	@FindBy(xpath = "//div/tr[1]/td[7]/span")
	public WebElement webelement_screenresultClasification;

	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div/div[4]/div[4]/div/div/div[1]/ul/li[3]/button")
	public WebElement webelement_activateScrining;
	@FindBy(xpath = "//div/tr[1]/td[1]/div")
	public WebElement webelement_ActivateScreeningSpinner;
	@FindBy(xpath = "//div/tr[1]/td[11]/span[1]/i")
	public WebElement webelement_FinanceCrime;
	@FindBy(xpath = "//div[@id='entity-company-content']/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div/div[4]/div[4]/div/div/div[1]/span")
	public WebElement webelement_SR_loading;
	@FindBy(xpath = "//div[@id='adversenewsModal']/div[1]")
	public WebElement webelement_adverseNewsModel_loading;
	@FindBy(xpath = "//div[@id='mTS_15_container']/div[1]/i")
	public WebElement webelement_anm_pepalerts;
	@FindBy(xpath = "//div[@id='adversenewsModal']/div[2]/button")
	public WebElement webelement_anm_close;

	@FindBy(xpath = "(//div[@id='p00'])[2]")
	public WebElement webelement_os_percentage;
	@FindBy(xpath = "//div[17]/ul/li[1]/span[2]/i")
	public WebElement webelement_os_addPerson;
	@FindBy(xpath = "//div[1]/div/div/div/div[2]/div/div/div/div[1]/form/div[1]/div[1]/input")
	public WebElement webelement_os_addp_firstname;
	@FindBy(xpath = "//div[1]/div/div/div/div[2]/div/div/div/div[1]/form/div[5]/button[3]")
	public WebElement webelement_os_addp_addbtn;

	@FindBy(xpath = "//div[@id='generateReportMainDiv']/div[1]/span/i")
	public WebElement webelement_top_sourceLink;
	@FindBy(xpath = "//div[20]/div[2]/div")
	public WebElement webelement_top_sourceOpened;
	@FindBy(xpath = "//div[@id='mCSB_120_container']/tr[2]/td[1]/div[1]/input")
	public WebElement webelement_top_source_berlin;
	@FindBy(xpath = "//div[@id='mCSB_120_container']/tr[3]/td[1]/div[1]/input")
	public WebElement webelement_top_source_frankfurt;
	@FindBy(xpath = "//div[@id='screen_shot']/div[2]/div/button")
	public WebElement webelement_top_source_addToPage;
	@FindBy(xpath = "//div[@id='entityClipboard']/a/span")
	public WebElement webelement_top_attach;
	@FindBy(xpath = "(//ul[@id='folder']/li[1]/span)[5]")
	public WebElement webelement_attach_berlin;
	@FindBy(xpath = "(//ul[@id='folder']/li[2]/span)[5]")
	public WebElement webelement_attach_frunkfurt;

	/*
	 * @FindBy(xpath="") public WebElement webelement_;
	 * 
	 * @FindBy(xpath="") public WebElement webelement_;
	 * 
	 * @FindBy(xpath="") public WebElement webelement_;
	 * 
	 * @FindBy(xpath="") public WebElement webelement_;
	 * 
	 * @FindBy(xpath="") public WebElement webelement_;
	 */

	public void switchingWindow(int i) throws InterruptedException {
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		System.out.println("size is : " + tabs2.size());
		driver.switchTo().window(tabs2.get(i));
		Thread.sleep(25000);
	}

	public void clickEntityType() {
		webelement_entityType.click();
	}

	public void clickEntityCompany() throws InterruptedException {
		while (!isDisplayed(webelement_entityCompany)) {
			Thread.sleep(3000);
			System.out.println("Financial is not visible yet");
		}
		webelement_entityCompany.click();
	}

	public void enterCompanyname(String companyname) throws InterruptedException {
		while (!isDisplayed(webelement_companyName)) {
			Thread.sleep(3000);
			System.out.println("Financial is not visible yet");
		}
		webelement_companyName.clear();
		webelement_companyName.sendKeys(companyname);
	}

	public void enterjurisdiction(String jurisdiction) {
		webelement_jurisdiction.clear();
		webelement_jurisdiction.sendKeys(jurisdiction);
		webelement_jurisdiction.sendKeys(Keys.TAB);

	}

	public void clickUpdate() {
		webelement_update.click();
	}

	public void clickSearch() {
		webelement_search.click();
	}

	public void clickFirstLink() throws InterruptedException {
		Thread.sleep(3000);
		webelement_firstLink.click();
	}

	public void clickFinancial() throws InterruptedException {
		while (!isDisplayed(webelement_financial)) {
			Thread.sleep(3000);
			System.out.println("Financial is not visible yet");
		}
		webelement_financial.click();
	}

	public void clickOverview() {
		webelement_overview.click();
	}

	public void clickCompliance() throws InterruptedException {
		while (!isDisplayed(webelement_Compliance)) {
			Thread.sleep(3000);
			System.out.println("Compliance is not visible yet");
		}
		webelement_Compliance.click();
	}

	public static boolean isDisplayed(WebElement element) {
		try {
			if (element.isDisplayed())
				return element.isDisplayed();
		} catch (NoSuchElementException ex) {
			return false;
		}
		return false;
	}

	public static boolean isClickable(WebElement element) {
		try {
			if (element.isEnabled())
				return element.isEnabled();
		} catch (NoSuchElementException ex) {
			return false;
		}
		return false;
	}

	public void clickLeadership() {
		webelement_Leadership.click();
	}

	public void clickRiskAlerts() {
		webelement_RiskAlerts.click();
	}

	public void clickLatestNews() {
		webelement_LatestNews.click();
	}

	public void clickThreatsIntelligence() {
		webelement_ThreatsIntelligence.click();
	}

	public void clickSocialMedia() {
		webelement_SocialMedia.click();
	}

	public void clickMedia() {
		webelement_Media.click();
	}

	public void clickFinancial1() {
		webelement_financial.click();
	}

	public int totalEntityTabs() {
		int totalli = driver
				.findElements(By
						.xpath("//div[@id='entity-company-content']/div[1]/div[1]/div/div/div[2]/div/div[1]/div/ul/li"))
				.size();
		return totalli;
	}

	public int checkDataNotFound(String arg) throws InterruptedException {

		Thread.sleep(30000);
		int i = 0, j = 0;

		String fullText = webelement_balanceSheet.getText();
		// System.out.println(fullText);

		if (fullText.equalsIgnoreCase("DATA NOT FOUND"))
			i = i + 1;
		else if (fullText.equalsIgnoreCase("LOADING..."))
			j = j + 1;

		fullText = webelement_cashFlow.getText();
		// System.out.println(fullText);
		if (fullText.equalsIgnoreCase("DATA NOT FOUND"))
			i = i + 1;
		else if (fullText.equalsIgnoreCase("LOADING..."))
			j = j + 1;
		fullText = webelement_incomeStmt.getText();
		// System.out.println(fullText);
		if (fullText.equalsIgnoreCase("DATA NOT FOUND"))
			i = i + 1;
		else if (fullText.equalsIgnoreCase("LOADING..."))
			j = j + 1;
		fullText = webelement_shareHolderType.getText();
		// System.out.println(fullText);
		// //*[@id="mCSB_13_container"]/div[5]/div/div[4]/div[2]/div
		if (fullText.equalsIgnoreCase("DATA NOT FOUND"))
			i = i + 1;
		else if (fullText.equalsIgnoreCase("LOADING..."))
			j = j + 1;
		fullText = webelement_totalRevenue.getText();
		// System.out.println(fullText);
		if (fullText.equalsIgnoreCase("DATA NOT FOUND"))
			i = i + 1;
		else if (fullText.equalsIgnoreCase("LOADING..."))
			j = j + 1;
		fullText = webelement_topHolders.getText();
		// System.out.println(fullText);
		if (fullText.equalsIgnoreCase("DATA NOT FOUND"))
			i = i + 1;
		else if (fullText.equalsIgnoreCase("LOADING..."))
			j = j + 1;
		fullText = webelement_shareHolderNetwork.getText();
		// System.out.println(fullText);
		if (fullText.equalsIgnoreCase("DATA NOT FOUND"))
			i = i + 1;
		else if (fullText.equalsIgnoreCase("LOADING..."))
			j = j + 1;
		fullText = webelement_stockHistory.getText();
		// System.out.println(fullText);
		if (fullText.equalsIgnoreCase("DATA NOT FOUND"))
			i = i + 1;
		else if (fullText.equalsIgnoreCase("LOADING..."))
			j = j + 1;

		// System.out.println("Total Data not founds are : "+ i);
		// System.out.println("Total Loadings are : " + j);

		if (arg.equalsIgnoreCase("data"))
			return i;
		else
			return j;

	}

	public int getstockHistory() {
		webelement_stockHistory.getText().contains("");
		return 1;
	}

	public void getCompanyName() throws InterruptedException {
		// webelement_cNameText.click();
		while (!isDisplayed(webelement_cNameText)) {
			Thread.sleep(3000);
			System.out.println("cname not visible yet");
		}
		System.out.println("company name on new window : " + webelement_cNameText.getText());
		Thread.sleep(3000);
	}

	public void clickSavedSearch() throws InterruptedException {
		while (!isDisplayed(webelement_savedSearch)) {
			Thread.sleep(3000);
			System.out.println("Saved search is not visible yet1");
		}
		webelement_savedSearch.click();
	}

	public String getSearchResultText() throws InterruptedException {
		while (!isDisplayed(webelement_searchResultHeader)) {
			Thread.sleep(3000);
			System.out.println("Element is not visible yet");
		}
		return webelement_searchResultHeader.getText();
	}

	public void clickRecentSearch() throws InterruptedException {
		// webelement_recentSearch .click();
		while (!isDisplayed(webelement_recentdiv)) {
			Thread.sleep(3000);
			System.out.println("Element is not visible yet");
		}
		webelement_recentdiv.findElement(By.xpath("//p[contains(.,'adidas')]")).click();
	}

	public void clickSaveSearch() throws InterruptedException {
		while (!isDisplayed(webelement_searchSave)) {
			Thread.sleep(3000);
			System.out.println("SaveSearch is not visible yet");
		}
		webelement_searchSave.click();
	}

	public String getSaveSuccessMessage() {
		return webelement_saveSuccessMessage.getText();
	}

	public void clickSearchCancel() throws InterruptedException {
		while (!isDisplayed(webelement_serachCancel)) {
			Thread.sleep(3000);
			System.out.println("search cancel is not visible yet");
		}
		webelement_serachCancel.click();
	}

	// OVERVIEW COMPONENTS
	public String getTextWhoAreWe() {
		return webelement_WhoAreWe.getText();
	}

	public String getTextLeadership() {
		return webelement_LeadershipText.getText();
	}

	public String getTextStockPerformance() {
		return webelement_StockPerformance.getText();
	}

	public String getTextVisualLinkAnalysis() {
		return webelement_VisualLinkAnalysis.getText();
	}

	public String getTextAssociatedCompanies() {
		return webelement_AssociatedCompanies.getText();
	}

	public String getTextContactAddress() {
		return webelement_ContactAddress.getText();
	}

	public String getTextOverviewRiskScore() {
		return webelement_RiskScore.getText();
	}

	public String getTextRiskRating() {
		return webelement_RiskRating.getText();
	}

	public String getTextRelatedEntities() {
		return webelement_RelatedEntities.getText();
	}

	public String getTextAssociatedLocations() {
		return webelement_AssociatedLocations.getText();
	}

	public String getTextLatestNews() {
		return webelement_O_LatestNews.getAttribute("innerText");
	}

	public String getTextAssociatedIndustries() {
		return webelement_AssociatedIndustries.getAttribute("innerText");
	}

	public String getTextDigitalFootPrint() {
		return webelement_DigitalFootPrint.getAttribute("innerText");
	}

	// compliance components
	public String getTextCompanyInformation() {
		return webelement_CompanyInformation.getAttribute("innerText");
	}

	public String getTextCompanyDetails() {
		return webelement_CompanyDetails.getAttribute("innerText");
	}

	public String getTextCompanyIdentifiers() {
		return webelement_CompanyIdentifiers.getAttribute("innerText");
	}

	public String getTextOwnershipConfigurations() {
		return webelement_OwnershipConfigurations.getAttribute("innerText");
	}

	public String getTextOwnershipStructure() {
		return webelement_OwnershipStructure.getAttribute("innerText");
	}

	public String getTextScreeningConfigurations() {
		return webelement_ScreeningConfigurations.getAttribute("innerText");
	}

	public String getTextScreeningResult() {
		return webelement_ScreeningResult.getAttribute("innerText");
	}

	public String getTextCountriesOfOperation() {
		return webelement_CountriesOfOperation.getAttribute("innerText");
	}

	public String getTextAssociatedDocuments() {
		return webelement_AssociatedDocuments.getAttribute("innerText");
	}

	// Leadership Components
	public String getTextKeyExecutive() {
		return webelement_KeyExecutive.getAttribute("innerText");
	}

	public String getTextBoardofDirectors() {
		return webelement_BoardofDirectors.getAttribute("innerText");
	}

	public String getTextLeadershipRiskScore() {
		return webelement_Leadership_RiskScore.getAttribute("innerText");
	}

	public String getTextCommittee() {
		return webelement_Committee.getAttribute("innerText");
	}

	public String getTextRatingTrend() {
		return webelement_RatingTrend.getAttribute("innerText");
	}

	public String getTextTOPINFLUENCERS() {
		return webelement_TOPINFLUENCERS.getAttribute("innerText");
	}

	public String getTextPeopleAlsoVisited() {
		return webelement_PeopleAlsoVisited.getAttribute("innerText");
	}

	public String getTextNearByCompanies() {
		return webelement_NearByCompanies.getAttribute("innerText");
	}

	public String getTextCompanyMembers() {
		return webelement_CompanyMembers.getAttribute("innerText");
	}

	public String getTextPositiveBusinessOutlook() {
		return webelement_PositiveBusinessOutlook.getAttribute("innerText");
	}

	public String getTextAssociatedPersons() {
		return webelement_AssociatedPersons.getAttribute("innerText");
	}

	public String getTextLeadershipClusters() {
		return webelement_LeadershipClusters.getAttribute("innerText");
	}

	public String getTextCareerOpportunities() {
		return webelement_CareerOpportunities.getAttribute("innerText");
	}

	// Financial Components
	public String getTextFinanaceRiskScore() {
		return webelement_f_RiskScore.getAttribute("innerText");
	}

	public String getTextTotalRevenuesVsYear() {
		return webelement_f_TotalRevenuesVsYear.getAttribute("innerText");
	}

	public String getTextShareholdersType() {
		return webelement_f_ShareholdersType.getAttribute("innerText");
	}

	public String getTextNetCashFlow() {
		return webelement_f_NetCashFlow.getAttribute("innerText");
	}

	public String getTextBalanceSheet() {
		return webelement_f_BalanceSheet.getAttribute("innerText");
	}

	public String getTextIncomeStatement() {
		return webelement_f_IncomeStatement.getAttribute("innerText");
	}

	public String getTextTopHolders() {
		return webelement_f_TopHolders.getAttribute("innerText");
	}

	public String getTextShareHolderNetwork() {
		return webelement_f_ShareHolderNetwork.getAttribute("innerText");
	}

	public String getTextStockHistory() {
		return webelement_f_StockHistory.getAttribute("innerText");
	}

	// Risk Alerts
	public String getTextAlertTimeline() {
		return webelement_AlertTimeline.getAttribute("innerText");
	}

	public String getTextPoliticalDonationsTrend() {
		return webelement_PoliticalDonationsTrend.getAttribute("innerText");
	}

	public String getTextRiskRiskScore() {
		return webelement_R_RiskScore.getAttribute("innerText");
	}

	public String getTextPoliticalRatio() {
		return webelement_PoliticalRatio.getAttribute("innerText");
	}

	public String getTextRiskRatio() {
		return webelement_RiskRatio.getAttribute("innerText");
	}

	public String getTextInterlocks() {
		return webelement_Interlocks.getAttribute("innerText");
	}

	public String getTextPepAlerts() {
		return webelement_PepAlerts.getAttribute("innerText");
	}

	public String getTextFraudAlerts() {
		return webelement_FraudAlerts.getAttribute("innerText");
	}

	public String getTextCyberAlerts() {
		return webelement_CyberAlerts.getAttribute("innerText");
	}

	public String getTextSanctionedList() {
		return webelement_SanctionedList.getAttribute("innerText");
	}

	public String getTextPepRelationshipChart() {
		return webelement_PepRelationshipChart.getAttribute("innerText");
	}

	public String getTextSuppliersBlackList() {
		return webelement_SuppliersBlackList.getAttribute("innerText");
	}

	public String getTextIndustrialSecurityIncidents() {
		return webelement_IndustrialSecurityIncidents.getAttribute("innerText");
	}

	public String getTextCriminalRecords() {
		return webelement_CriminalRecords.getAttribute("innerText");
	}

	public String getTextDarkWeb() {
		return webelement_DarkWeb.getAttribute("innerText");
	}

	public String getTextNews() {
		return webelement_News.getAttribute("innerText");
	}

	// Latest News
	public String getTextOverallSentiment() {
		return webelement_OverallSentiment.getAttribute("innerText");
	}

	public String getTextNewsRiskScore() {
		return webelement_News_RiskScore.getAttribute("innerText");
	}

	public String getTextGeneralSentiments() {
		return webelement_GeneralSentiments.getAttribute("innerText");
	}

	public String getTextNewsLatestNews() {
		return webelement_News_LatestNews.getAttribute("innerText");
	}

	public String getTextPressReleases() {
		return webelement_PressReleases.getAttribute("innerText");
	}

	public String getTextPopularTags() {
		return webelement_PopularTags.getAttribute("innerText");
	}

	public String getTextRecentEvents() {
		return webelement_RecentEvents.getAttribute("innerText");
	}

	public String getTextRelationshipChartsByNews() {
		return webelement_RelationshipChartsByNews.getAttribute("innerText");
	}

	public String getTextNewsLocations() {
		return webelement_NewsLocations.getAttribute("innerText");
	}

	public void clickCheckList() throws InterruptedException {
		while (!isDisplayed(webelement_CheckList)) {
			Thread.sleep(3000);
			System.out.println("CheckList is not visible yet");
		}
		webelement_CheckList.click();

	}

	public void clickNoAnswer() {
		webelement_noAnswer1.click();
	}

	public void clickNextButton(int i) throws InterruptedException {
		for (int j = 0; j < i; j++) {
			getspanner();
			// Thread.sleep(3000);
			webelement_NextButton.click();
		}
	}

	public void legalExistence() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_noAnswer1.click();
		webelement_NextButton.click();
	}

	public void customerProductInformation() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_noAnswer1.click();
		webelement_NextButton.click();
	}

	public void customerActivityHighRisk() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_noAnswer1.click();
		webelement_NextButton.click();
	}

	public void ownershipStructure() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_rbNo1.click();
		webelement_rbNo2.click();
		webelement_ta_os.clear();
		webelement_ta_os.sendKeys("test");
		webelement_NextButton.click();
	}

	public void listingRegulationStatus() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_noAnswer1.click();
		webelement_noAnswer2.click();
		webelement_noAnswer3.click();
		webelement_ta_lr.clear();
		webelement_ta_lr.sendKeys("test");
		webelement_NextButton.click();
	}

	public void complexOwnershipStructure() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_rbNo3.click();
		webelement_NextButton.click();
	}

	public void UBO() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_rbNo4.click();
		webelement_noAnswer1.click();
		// webelement_noAnswer2.click();
		webelement_NextButton.click();
	}

	public void mainPrincipals() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_rbNo5.click();
		webelement_textArea1.clear();
		webelement_textArea1.sendKeys("test");
		webelement_NextButton.click();
	}

	public void associatedParties() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_rbNo6.click();
		webelement_NextButton.click();
	}

	public void Principals() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_noAnswer1.click();
		webelement_NextButton.click();
	}

	public void sourceofFunds() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_rbNo7.click();
		webelement_textArea2.clear();
		webelement_textArea2.sendKeys("test");
		webelement_NextButton.click();
	}

	public void Screening() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_rbNo8.click();
		webelement_NextButton.click();
	}

	public void INITIAL_RISK_RATING() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_rbNo9.click();
		webelement_rbNo10.click();
		webelement_rbNo11.click();
		webelement_rbNo12.click();
		webelement_noAnswer1.click();
		webelement_rbNo13.click();
		webelement_noAnswer2.click();
		webelement_NextButton.click();
	}

	public void UHRC() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_noAnswer1.click();
		webelement_NextButton.click();
	}

	public void profileSummary() throws InterruptedException {
		getspanner();
		// Thread.sleep(3000);
		webelement_textArea3.clear();
		webelement_textArea3.sendKeys("test");
		webelement_textArea4.clear();
		webelement_textArea4.sendKeys("test");
		webelement_textArea5.clear();
		webelement_textArea5.sendKeys("test");
		webelement_textArea6.clear();
		webelement_textArea6.sendKeys("test");
		webelement_textArea7.clear();
		webelement_textArea7.sendKeys("test");
		webelement_textArea8.clear();
		webelement_textArea8.sendKeys("test");
		webelement_textArea9.clear();
		webelement_textArea9.sendKeys("test");
		webelement_textArea10.clear();
		webelement_textArea10.sendKeys("test");
		webelement_textArea11.clear();
		webelement_textArea11.sendKeys("test");
		webelement_textArea12.clear();
		webelement_textArea12.sendKeys("test");
		webelement_textArea13.clear();
		webelement_textArea13.sendKeys("test");
		webelement_textArea14.clear();
		webelement_textArea14.sendKeys("test");
		webelement_textArea15.clear();
		webelement_textArea15.sendKeys("test");
		webelement_textArea16.clear();
		webelement_textArea16.sendKeys("test");
		webelement_textArea17.clear();
		webelement_textArea17.sendKeys("test");
		webelement_textArea18.clear();
		webelement_textArea18.sendKeys("test");
		webelement_textArea19.clear();
		webelement_textArea19.sendKeys("test");
		webelement_textArea20.clear();
		webelement_textArea20.sendKeys("test");
		webelement_textArea21.clear();
		webelement_textArea21.sendKeys("test");
		webelement_textArea22.clear();
		webelement_textArea22.sendKeys("test");
		webelement_textArea23.clear();
		webelement_textArea23.sendKeys("test");
		webelement_textArea24.clear();
		webelement_textArea24.sendKeys("test");
		webelement_textArea25.clear();
		webelement_textArea25.sendKeys("test");
		webelement_textArea26.clear();
		webelement_textArea26.sendKeys("test");
		webelement_textArea27.clear();
		webelement_textArea27.sendKeys("test");
		while (!isDisplayed(webelement_SubmitButton)) {
			Thread.sleep(3000);
			System.out.println("SubmitButton is not visible yet2");
		}
		webelement_SubmitButton.click();

	}

	public void clickSubmitButton() throws InterruptedException {
		getspanner();
		while (!isDisplayed(webelement_SubmitButton)) {
			Thread.sleep(3000);
			System.out.println("SubmitButton is not visible yet2");
		}
		webelement_SubmitButton.click();
	}

	public String getThankyouMassege() {
		return webelement_ThankYouMessage.getText();
	}

	public void clickGenerateReport() {
		webelement_generateReport.click();
	}

	public String getReportTableOfContents() {
		System.out.println("hooo");
		System.out.println(driver.getTitle());
		// return webelement_ReportTableOfContent.getText();
		return "not have data";
	}

	public void getspanner() throws InterruptedException {
		while (webelement_spinner.getAttribute("style").contains("display: flex;")) {
			Thread.sleep(1000);
			System.out.println("spanner is not visible yet3");
		}
	}

	public void clickOnBoarding() {
		webelement_onBoarding.click();
	}

	public String getOnBoardingTesxt() {
		return webelement_OnboardingText.getText();
	}

	public void clickonboardingclose() {
		webelement_OnboardingClose.click();
	}

	public void clickScreeningResult() throws InterruptedException {

		while (webelement_ScreeningResultAdd.getAttribute("class").contains("c-ban")) {
			Thread.sleep(3000);
			System.out.println("+ Add is not visible yet");

			/*
			 * count=count+1; if(count > 100) { break; }
			 */
		}
		webelement_ScreeningResultAdd.click();
	}

	public void typeFirstName() throws InterruptedException {
		while (!isDisplayed(webelement_sr_firstName)) {
			Thread.sleep(3000);
			System.out.println("First Name is not visible yet");
		}
		webelement_sr_firstName.sendKeys("fName");
	}

	public void typeLastName() {
		webelement_sr_lastName.sendKeys("lName");
	}

	public void typeCompanyName() {
		webelement_sr_companyName.sendKeys("Russia");
		webelement_sr_companyName.sendKeys(Keys.ARROW_DOWN);
		webelement_sr_companyName.sendKeys(Keys.ENTER);
	}

	public void typeDateOfBirth() {
		webelement_sr_dob.sendKeys("2000-10-26");
	}

	public void clickSRSave() {
		webelement_sr_save.click();
	}

	public void clickExpand() throws InterruptedException {
		System.out.println("ttt " + webelement_os_text.getText());
		while (!isDisplayed(webelement_os_expand)) {
			Thread.sleep(3000);
			System.out.println("Expand is not visible yet");
		}
		// webelement_os_expand.click();
		// WebElement element =
		// driver.findElement(By.xpath("//div[@id='flowChartViewDiv1']/div[2]/ul/li[2]/a"));
		Actions actions = new Actions(driver);
		actions.moveToElement(webelement_os_expand).click().build().perform();
		// webelement_os_expand.sendKeys(Keys.SPACE);
		// webelement_os_expand.click();
	}

	public String getExpandModelText() throws InterruptedException {
		while (!isDisplayed(webelement_os_expandModalText)) {
			Thread.sleep(3000);
			System.out.println("expandModalText is not visible yet");
		}
		return webelement_os_expandModalText.getText();
	}

	public void screenResultLoading() throws InterruptedException {
		while (!webelement_ScreeningResultLoading.getAttribute("class").contains("custom-spinner ng-hide")) {
			Thread.sleep(3000);
			System.out.println("Loading Please wait,,");
			/*
			 * count=count+1; if(count > 140) { break; }
			 */
		}

	}

	public String getTextAddedScreenResult() throws InterruptedException {
		// webelement_sr_addedResultClick.click();
		// webelement_sr_delete.click();
		// webelement_sr_deleteCancel.click();
		// webelement_sr_addedResultClick.click();

		Point hoverItem = driver.findElement(By.xpath("(//td/span[contains(.,'fName lName')])[1]")).getLocation();

		System.out.println("location details : " + hoverItem);
		((JavascriptExecutor) driver).executeScript("return window.title;");
		Thread.sleep(6000);
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0," + (hoverItem.getY()) + ");");

		/*
		 * for (int i=0; i<15;i++) { Thread.sleep(2000);
		 * 
		 * webelement_sr_addedResultClick.sendKeys(Keys.PAGE_DOWN); }
		 */

		return webelement_sr_addedResult.getAttribute("innerText");
	}

	public void clickExpandClose() {
		webelement_os_expandClose.click();
	}

	public void ownershipstructureLoading() throws InterruptedException {
		loadingPrasent(webelement_os_loading);

	}

	public void loadingPrasent(WebElement webelement_os_loading2) throws InterruptedException {
		while (!webelement_os_loading.getAttribute("class").contains("ng-hide")) {
			Thread.sleep(3000);
			System.out.println("Still Loading..");
		}
	}

	public void clickAdvanceSearch() throws InterruptedException {
		while (!isDisplayed(webelement_advanceSearch)) {
			Thread.sleep(3000);
			System.out.println("adv search is not visible yet");
		}

		webelement_advanceSearch.click();
	}

	public void clickSocialListning() throws InterruptedException {
		System.out.println(webelement_socialListningLink.getAttribute("innerText"));
		while (!isDisplayed(webelement_socialListningLink)) {
			Thread.sleep(3000);
			System.out.println("social Listning not visible yet");
		}

		webelement_socialListningLink.click();
	}

	public String getHeaderText() {
		return webelement_headerText.getText();
	}

	// compliance components
	public String getTextPeerGroup() {
		return webelement_ti_peerGroup.getAttribute("innerText");
	}

	public String getTextsimilarcompanies() {
		return webelement_ti_similarcompanies.getAttribute("innerText");
	}

	public String getTextThreatsriskscore() {
		return webelement_ti_riskscore.getAttribute("innerText");
	}

	public String getTextbytopcountries() {
		return webelement_ti_bytopcountries.getAttribute("innerText");
	}

	public String getTextbytoptechnologies() {
		return webelement_ti_bytoptechnologies.getAttribute("innerText");
	}

	public String getTextcybersecurity() {
		return webelement_ti_cybersecurity.getAttribute("innerText");
	}

	public String getTextselectlocation() {
		return webelement_ti_selectlocation.getAttribute("innerText");
	}

	public String getTextselectindustry() {
		return webelement_ti_selectindustry.getAttribute("innerText");
	}

	public String getTextdisplaying() {
		return webelement_ti_displaying.getAttribute("innerText");
	}

	public String getTexttagcloud() {
		return webelement_ti_tagcloud.getAttribute("innerText");
	}

	public String getTextthreattimeline() {
		return webelement_ti_threattimeline.getAttribute("innerText");
	}

	public String getTextIncidentsClassificationByIndustry() {
		return webelement_ti_IncidentsClassificationByIndustry.getAttribute("innerText");
	}

	public String getTextincidentsclassification() {
		return webelement_ti_incidentsclassification.getAttribute("innerText");
	}

	public String getTextrelationships() {
		return webelement_ti_relationships.getAttribute("innerText");
	}

	// Social Media
	public String getTextActivityFeed() {
		return webelement_sm_ActivityFeed.getAttribute("innerText");
	}

	public String getTextRecentTweets() {
		return webelement_sm_RecentTweets.getAttribute("innerText");
	}

	public String getTextInstagramPosts() {
		return webelement_sm_InstagramPosts.getAttribute("innerText");
	}

	public String getTextFacebookSocialFeeds() {
		return webelement_sm_FacebookSocialFeeds.getAttribute("innerText");
	}

	public String getTextRecentLinkedinPost() {
		return webelement_sm_RecentLinkedinPost.getAttribute("innerText");
	}

	public String getTextGooglePlusPost() {
		return webelement_sm_GooglePlusPost.getAttribute("innerText");
	}

	public String getTextLiveFeed() {
		return webelement_sm_LiveFeed.getAttribute("innerText");
	}

	public String getTextSocialMediaRISKSCORE() {
		return webelement_sm_RISKSCORE.getAttribute("innerText");
	}

	public String getTextACTIVITYLOCATIONS() {
		return webelement_sm_ACTIVITYLOCATIONS.getAttribute("innerText");
	}

	public String getTextTWITTERTAGWORDS() {
		return webelement_sm_TWITTERTAGWORDS.getAttribute("innerText");
	}

	public String getTextINTERACTIONRATIO() {
		return webelement_sm_INTERACTIONRATIO.getAttribute("innerText");
	}

	// Media
	public String getTextImages() {
		return webelement_m_images.getAttribute("innerText");
	}

	public String getTextMediaRiskScore() {
		return webelement_m_RiskScore.getAttribute("innerText");
	}

	public void clickownershipControlpercentage() {
		webelement_ownershipControlpercentage.click();
	}

	public void clickOwnershipConfigurationApply() {
		webelement_ownershipConfigurationApply.click();
	}

	public void clickpepAlert() {
		webelement_pepslider.click();
	}

	public void clickSanctionsConfidenceSlider() {
		webelement_sanctionsConfidentslider.click();
	}

	public void clicknewsPeriodSlider() {
		webelement_newsperiodSlider.click();
	}

	public void clickScreenConfigurationApply() {
		webelement_screeningConfigurationApply.click();
	}

	public void clickScreenresultCheckBox() throws InterruptedException {
		// Thread.sleep(3000);
		// *[@id="entity-company-content"]/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div/div[4]/div[4]/div/div/div[1]/span
		loadingPrasent(webelement_SR_loading);
		webelement_screenresultResidance.click();
		// webelement_screenresultDOB.click();
		// webelement_screenresultCompany.click();
		Actions action = new Actions(driver);
		action.moveToElement(webelement_screenresultCheckbox).click().build().perform();
		// action.moveToElement(webelement_screenresultCheckbox).build().perform();
		// webelement_screenresultCheckbox.click();
	}

	public void clickActivateScreen() throws InterruptedException {
		while (!isDisplayed(webelement_activateScrining)) {
			Thread.sleep(3000);
			System.out.println("Activate Screening not visible yet");
		}
		webelement_activateScrining.click();
	}

	public void waitActivteScreenSpinner() throws InterruptedException {
		/*
		 * while(!webelement_ActivateScreeningSpinner.getAttribute("class").contains(
		 * "checkbox")) { Thread.sleep(3000); System.out.println("Still Loading..."); }
		 */
	}

	public void clickFinanceCrime() throws InterruptedException {
		loadingWait(webelement_FinanceCrime, "FinanceCrime is not visible yet");
		webelement_FinanceCrime.click();
	}

	public void loadingWait(WebElement ele, String str) throws InterruptedException {
		count = 0;
		while (!isDisplayed(webelement_FinanceCrime)) {
			if (count < 20) {
				Thread.sleep(3000);
				System.out.println("FinanceCrime is not visible yet");
			} else
				break;
			count += 1;
		}
	}

	public void waitAdverceNewsModel() throws InterruptedException {
		loadingPrasent(webelement_adverseNewsModel_loading);
	}

	public String getAdverseNewsModelText(int i) throws InterruptedException {
		WebElement ele = driver.findElement(By.xpath("//div/h4/a/span/div/div[2]/ul/li[" + i + "]/span"));
		return ele.getText();
	}

	public void movetoElement(WebElement ele) {
		Actions action = new Actions(driver);
		action.moveToElement(ele).perform();
	}

	public void clickANMClose() {
		webelement_anm_close.click();
	}

	public void clickOSCompanyname() throws InterruptedException {
		Thread.sleep(3000);
		while (!isDisplayed(webelement_os_percentage)) {
			Thread.sleep(3000);
			System.out.println("adv search is not visible yet");
		}
		movetoElement(webelement_os_percentage);
		webelement_os_percentage.click();
	}

	public void clickOSAddPerson() throws InterruptedException {
		Thread.sleep(3000);
		while (!isDisplayed(webelement_os_addPerson)) {
			Thread.sleep(3000);
			System.out.println("AddPerson is not visible yet");
		}
		movetoElement(webelement_os_addPerson);
		webelement_os_addPerson.click();
	}

	public void typeOSFirstName() throws InterruptedException {
		while (!isDisplayed(webelement_os_addp_firstname)) {
			Thread.sleep(3000);
			System.out.println("adv search is not visible yet");
		}
		webelement_os_addp_firstname.sendKeys("TestPerson");
	}

	public void clickOSAdd() throws InterruptedException {
		while (!isDisplayed(webelement_os_addp_addbtn)) {
			Thread.sleep(3000);
			System.out.println("adv search is not visible yet");
		}
		webelement_os_addp_addbtn.click();
	}

	public void clicSourceLink() throws InterruptedException {
		while (!isDisplayed(webelement_top_sourceLink)) {
			Thread.sleep(3000);
			System.out.println("adv search is not visible yet");
		}
		webelement_top_sourceLink.click();
	}

	public void clickSourceberlin() throws InterruptedException {

		while (!isDisplayed(webelement_top_sourceOpened)) {
			Thread.sleep(3000);
			System.out.println("berlin is not visible yet");
		}
		// movetoElement(webelement_top_source_berlin);
		webelement_top_source_berlin.click();
	}

	public void clickSourcefrankfurt() throws InterruptedException {
		while (!isDisplayed(webelement_top_sourceOpened)) {
			Thread.sleep(3000);
			System.out.println("frankfurt is not visible yet");
		}
		webelement_top_source_frankfurt.click();
	}

	public void clickSourceAddToPage() throws InterruptedException {
		while (!isDisplayed(webelement_top_source_addToPage)) {
			Thread.sleep(3000);
			System.out.println("adv search is not visible yet");
		}
		webelement_top_source_addToPage.click();
	}

	public void cickTopAttach() {
		webelement_top_attach.click();
	}

	public void clickberlin() {
		webelement_attach_berlin.click();
	}
}
