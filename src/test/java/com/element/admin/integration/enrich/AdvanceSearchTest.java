package com.element.admin.integration.enrich;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;

import com.element.admin.integration.enrich.EnrichPage;
import com.element.admin.integration.aml.AmlPage;
import com.element.admin.integration.common.Login;
import com.element.admin.integration.common.Util;
import com.element.admin.integration.shared.SharedPage;

public class AdvanceSearchTest {
	WebDriver driver;
	String url;
	Util util;
	String username;
	String password;
	String companyname;
	String jurisdiction;
	String actual = "", expected = "";

	/**
	 * Sets the up value.
	 */

	@BeforeClass
	public void setUp() {
		final Login login = new Login(driver);
		driver = login.setup();
	}

	@Test(priority = 2)
	public void domain() {
		final AmlPage aml = new AmlPage(driver);
		aml.clickAML();
	}

	@Test(priority = 3)
	public void process() {
		final SharedPage spage = new SharedPage(driver);
		spage.clickAdvanceSearch();
		// 48 Demo Sanity Test
	}

	@Test(enabled = false)
	public void TS_004_08() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickEntityType();
		enrich.clickEntityCompany();
		enrich.enterCompanyname(companyname);
		enrich.enterjurisdiction(jurisdiction);
		enrich.clickUpdate();
		enrich.clickSearch();
		// clickFirstResult();
		// 47 Demo Sanity Test
	}

	@Test(priority = 6)
	public void clickFirstResult() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickFirstLink();
	}

	@Test(priority = 4)
	public void TS_004_10() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickSavedSearch();
		Thread.sleep(3000);
		// System.out.println("save search reuslts are :
		// "+enrich.getSearchResultText());
		enrich.clickSearchCancel();
		// 49 Demo Sanity Test
	}

	@Test(priority = 5)
	public void TS_004_09() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickRecentSearch();
		Thread.sleep(3000);
		enrich.getSearchResultText();
		enrich.clickSaveSearch();
		Thread.sleep(3000);
		// System.out.println(enrich.getSaveSuccessMessage());
		// enrich.clickSearchCancel();
		// 48 Demo Sanity Test
	}

	@Test(priority = 7)
	public void switchToNewWindow() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		Thread.sleep(25000);
		enrich.clickFinancial();
	}

	@Test(enabled = false)
	public void dataNotFoundCount() throws InterruptedException {
		Thread.sleep(15000);

		final EnrichPage enrich = new EnrichPage(driver);

		enrich.getCompanyName();
		int i = enrich.checkDataNotFound("data");
		System.out.println("total data not founds are  : " + i);
		i = enrich.checkDataNotFound("loading..");
		System.out.println("total loadings are  : " + i);
	}

	@Test(priority = 8)
	public void TS_004_11() throws InterruptedException {

		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickOverview();
		Thread.sleep(5000);

		expected = "Who Are We";
		actual = enrich.getTextWhoAreWe();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Leadership";
		actual = enrich.getTextLeadership();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Stock Performance";
		actual = enrich.getTextStockPerformance();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Visual Link Analysis";
		actual = enrich.getTextVisualLinkAnalysis();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Associated Companies";
		actual = enrich.getTextAssociatedCompanies();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "CONTACT ADDRESS";
		actual = enrich.getTextContactAddress();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "RISK SCORE";
		actual = enrich.getTextOverviewRiskScore();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "RISK RATINGS";
		actual = enrich.getTextRiskRating();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "RELATED ENTITIES";
		actual = enrich.getTextRelatedEntities();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "ASSOCIATED LOCATIONS";
		actual = enrich.getTextAssociatedLocations();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "LATEST NEWS";
		actual = enrich.getTextLatestNews();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "ASSOCIATED INDUSTRIES";
		actual = enrich.getTextAssociatedIndustries();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "DIGITAL FOOTPRINTS";
		actual = enrich.getTextDigitalFootPrint();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		// 50 Demo Sanity Test
	}

	@Test(priority = 9)
	public void TS_004_12() throws InterruptedException {

		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickCompliance();
		Thread.sleep(5000);

		expected = "Company Information";
		actual = enrich.getTextCompanyInformation();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Company Details";
		actual = enrich.getTextCompanyDetails();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Company Identifiers";
		actual = enrich.getTextCompanyIdentifiers();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Ownership Configurations:";
		actual = enrich.getTextOwnershipConfigurations();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Ownership Structure ";
		actual = enrich.getTextOwnershipStructure();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Screening Configurations:";
		actual = enrich.getTextScreeningConfigurations();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Screening Results ";
		actual = enrich.getTextScreeningResult();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "COUNTRIES OF OPERATION";
		actual = enrich.getTextCountriesOfOperation();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "ASSOCIATED DOCUMENTS";
		actual = enrich.getTextAssociatedDocuments();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		// 51 Demo Sanity Test
	}

	@Test(priority = 10)
	public void TS_004_13() throws InterruptedException {

		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickLeadership();
		Thread.sleep(5000);

		expected = "Key Executive";
		actual = enrich.getTextKeyExecutive();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Board of Directors";
		actual = enrich.getTextBoardofDirectors();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Committee";
		actual = enrich.getTextCommittee();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "RISK SCORE";
		actual = enrich.getTextLeadershipRiskScore();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "RATING TREND";
		actual = enrich.getTextRatingTrend();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "TOP INFLUENCERS";
		actual = enrich.getTextTOPINFLUENCERS();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "People Also Visited";
		actual = enrich.getTextPeopleAlsoVisited();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Near By Companies";
		actual = enrich.getTextNearByCompanies();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Company Members";
		actual = enrich.getTextCompanyMembers();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Positive Business Outlook";
		actual = enrich.getTextPositiveBusinessOutlook();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "CAREER OPPORTUNITIES";
		actual = enrich.getTextCareerOpportunities();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Leadership Clusters";
		actual = enrich.getTextLeadershipClusters();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Associated Persons";
		actual = enrich.getTextAssociatedPersons();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		// 52 Demo Sanity Test
	}

	@Test(priority = 11)
	public void TS_004_14() throws InterruptedException {

		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickFinancial();
		Thread.sleep(5000);

		expected = "RISK SCORE";
		actual = enrich.getTextFinanaceRiskScore();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "TOTAL REVENUES VS YEAR";
		actual = enrich.getTextTotalRevenuesVsYear();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "SHAREHOLDERS TYPE";
		actual = enrich.getTextShareholdersType();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Stock History";
		actual = enrich.getTextStockHistory();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Net Cash Flow - Operating Activities";
		actual = enrich.getTextNetCashFlow();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "Balance Sheet";
		actual = enrich.getTextBalanceSheet();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "INCOME STATEMENT";
		actual = enrich.getTextIncomeStatement();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "TOP HOLDERS";
		actual = enrich.getTextTopHolders();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "SHARE HOLDER NETWORK";
		actual = enrich.getTextShareHolderNetwork();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		// 53 Demo Sanity Test
	}

	@Test(priority = 12)
	public void TS_004_15() throws InterruptedException {

		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickRiskAlerts();
		Thread.sleep(5000);

		expected = "Alert Timeline";
		actual = enrich.getTextAlertTimeline();
		// System.out.println(actual);
		AssertJUnit.assertTrue(actual.contains(expected));

		expected = "RISK SCORE";
		actual = enrich.getTextRiskRiskScore();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "POLITICAL DONATIONS TREND";
		actual = enrich.getTextPoliticalDonationsTrend();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "PEP ALERTS";
		actual = enrich.getTextPepAlerts();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "FRAUD ALERTS";
		actual = enrich.getTextFraudAlerts();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "CYBER ALERTS";
		actual = enrich.getTextCyberAlerts();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "POLITICAL RATIO";
		actual = enrich.getTextPoliticalRatio();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "SANCTIONED LIST";
		actual = enrich.getTextSanctionedList();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "NEWS";
		actual = enrich.getTextNews();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "PEP RELATIONSHIP CHART";
		actual = enrich.getTextPepRelationshipChart();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "RISK RATIO";
		actual = enrich.getTextRiskRatio();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "INTERLOCKS";
		actual = enrich.getTextInterlocks();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "SUPPLIER'S BLACK LIST";
		actual = enrich.getTextSuppliersBlackList();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "INDUSTRIAL SECURITY INCIDENTS";
		actual = enrich.getTextIndustrialSecurityIncidents();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "CRIMINAL RECORDS";
		actual = enrich.getTextCriminalRecords();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "DARK WEB";
		actual = enrich.getTextDarkWeb();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		// 54 Demo Sanity Test
	}

	@Test(priority = 13)
	public void TS_004_17() throws InterruptedException {

		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickLatestNews();
		Thread.sleep(5000);

		expected = "Overall Sentiment";
		actual = enrich.getTextOverallSentiment();
		AssertJUnit.assertTrue(actual.contains(expected));

		expected = "RISK SCORE";
		actual = enrich.getTextNewsRiskScore();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "GENERAL SENTIMENTS";
		actual = enrich.getTextGeneralSentiments();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "LATEST NEWS";
		actual = enrich.getTextNewsLatestNews();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "PRESS RELEASES";
		actual = enrich.getTextPressReleases();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "POPULAR TAGS";
		actual = enrich.getTextPopularTags();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "RECENT EVENTS";
		actual = enrich.getTextRecentEvents();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "RELATIONSHIP CHARTS BY NEWS";
		actual = enrich.getTextRelationshipChartsByNews();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected = "NEWS LOCATIONS";
		actual = enrich.getTextNewsLocations();
		AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		// 56 Demo Sanity Test
	}

	@Test(priority = 14)
	public void TS_004_16() throws InterruptedException {

		final EnrichPage enrich = new EnrichPage(driver);

		if (enrich.totalEntityTabs() <= 6) {
			throw new SkipException("Skipping because Manage --> System Settings, selected other than the Complance ");
		} else {

			enrich.clickThreatsIntelligence();
			Thread.sleep(5000);

			expected = "Peer Group";
			actual = enrich.getTextPeerGroup();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected = "Similar Companies";
			actual = enrich.getTextsimilarcompanies();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected = "Risk Score";
			actual = enrich.getTextThreatsriskscore();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected = "By Top-Countries";
			actual = enrich.getTextbytopcountries();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected = "By Top Technologies";
			actual = enrich.getTextbytoptechnologies();
			System.out.println("??" + actual);
			AssertJUnit.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));
			expected = "Cyber Security";
			actual = enrich.getTextcybersecurity();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected = "Select Location";
			actual = enrich.getTextselectlocation();
			AssertJUnit.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));
			expected = "Select Industry";
			actual = enrich.getTextselectindustry();
			AssertJUnit.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));
			expected = "displaying :";
			actual = enrich.getTextdisplaying();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected = "Tag Cloud";
			actual = enrich.getTexttagcloud();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected = "Threat Timeline";
			actual = enrich.getTextthreattimeline();
			AssertJUnit.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));
			expected = "Incidents Classification By Industry";
			actual = enrich.getTextIncidentsClassificationByIndustry();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected = "Incidents Classification";
			actual = enrich.getTextincidentsclassification();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected = "Relationships";
			actual = enrich.getTextrelationships();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		}
	}

	@Test(priority = 15)
	public void TS_004_18() throws InterruptedException {

		final EnrichPage enrich = new EnrichPage(driver);

		if (enrich.totalEntityTabs() <= 6) {
			throw new SkipException("Skipping because Manage --> System Settings, selected other than the Complance ");
		} else {

			enrich.clickSocialMedia();
			Thread.sleep(5000);

			expected = "Activity Feed";
			actual = enrich.getTextActivityFeed();
			AssertJUnit.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));

			expected = "Recent Tweets";
			actual = enrich.getTextRecentTweets();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

			expected = "Instagram Posts";
			actual = enrich.getTextInstagramPosts();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

			expected = "Facebook Social Feeds";
			actual = enrich.getTextFacebookSocialFeeds();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

			expected = "Recent Linkedin Post";
			actual = enrich.getTextRecentLinkedinPost();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

			expected = "Google Plus Post";
			actual = enrich.getTextGooglePlusPost();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

			expected = "Live Feed";
			actual = enrich.getTextLiveFeed();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

			expected = "RISK SCORE";
			actual = enrich.getTextSocialMediaRISKSCORE();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

			expected = "ACTIVITY LOCATIONS";
			actual = enrich.getTextACTIVITYLOCATIONS();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

			expected = "TWITTER TAG WORDS";
			actual = enrich.getTextTWITTERTAGWORDS();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());

			expected = "INTERACTION RATIO";
			actual = enrich.getTextINTERACTIONRATIO();
			AssertJUnit.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		}
	}

	@Test(priority = 16)
	public void TS_004_19() throws InterruptedException {

		final EnrichPage enrich = new EnrichPage(driver);

		if (enrich.totalEntityTabs() <= 6) {
			throw new SkipException("Skipping because Manage --> System Settings, selected other than the Complance ");
		} else {

			enrich.clickMedia();
			Thread.sleep(5000);

			expected = "Images";
			actual = enrich.getTextImages();
			AssertJUnit.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));

			expected = "Risk Score";
			actual = enrich.getTextMediaRiskScore();
			AssertJUnit.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));
		}
	}

	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}
}
