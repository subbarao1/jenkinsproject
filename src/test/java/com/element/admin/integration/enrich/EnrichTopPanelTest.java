package com.element.admin.integration.enrich;

import org.testng.annotations.Test;

import com.element.admin.integration.aml.AmlPage;
import com.element.admin.integration.common.Login;
import com.element.admin.integration.shared.SharedPage;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;

public class EnrichTopPanelTest {
	WebDriver driver;
	String actual = "", expected = "";

	@BeforeClass
	public void startup() {
		final Login login = new Login(driver);
		driver = login.setup();
	}

	@Test(priority = 1)
	public void domain() {
		final AmlPage aml = new AmlPage(driver);
		aml.clickAML();
	}

	@Test(priority = 2)
	public void searchCompany() throws InterruptedException {
		final SharedPage spage = new SharedPage(driver);
		spage.clickAdvanceSearch();
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clickSavedSearch();
		Thread.sleep(3000);
		enrich.clickFirstLink();
		enrich.switchingWindow(1);
	}

	@Test(priority = 3)
	public void TS_004_01() throws InterruptedException {
		final EnrichPage enrich = new EnrichPage(driver);
		enrich.clicSourceLink();
		enrich.clickSourceberlin();
		enrich.clickSourcefrankfurt();
		enrich.clickSourceAddToPage();
		enrich.cickTopAttach();

	}

	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}
}
