package com.element.admin.integration.manage;

import java.awt.AWTException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.Select;

import com.element.admin.integration.common.Util;

public class ManagePage {

	public WebDriver driver;
	Util util;

	public int DRIVER_WAIT = 30; // 30 seconds

	/**
	 * Constructor.
	 *
	 * @param driver an instance of WebDriver
	 */
	public ManagePage(final WebDriver driver) {
		final ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, DRIVER_WAIT);
		PageFactory.initElements(finder, this);
		this.driver = driver;
		util = new Util();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
	}

	// bdsp
	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[5]/button/p")
	public WebElement webelement_bdsp;
	@FindBy(xpath = "//a[@id='createNewWorkflow']")
	public WebElement webelement_createNewWorkflow;
	@FindBy(xpath = "//input[@id='workflowName']")
	public WebElement webelement_workflowName;
	@FindBy(xpath = "//button[@id='workflowcreatenew']")
	public WebElement webelement_workflowcreatenew;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[1]/div/a/i")
	public WebElement webelement_bdsp_sources;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[2]/div/a/i")
	public WebElement webelement_bdsp_operations;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[3]/div/a/i")
	public WebElement webelement_bdsp_analytic;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[4]/div/a/i")
	public WebElement webelement_bdsp_gissensivity;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[5]/div/a/i")
	public WebElement webelement_bdsp_iot;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[6]/div/a/i")
	public WebElement webelement_bdsp_sinks;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[7]/div/a/i")
	public WebElement webelement_bdsp_domain;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[8]/div/a/i")
	public WebElement webelement_bdsp_industries;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[9]/div/a/i")
	public WebElement webelement_bdsp_graphs;
	@FindBy(xpath = "//div[@id='mCSB_19_container']/h5")
	public WebElement webelement_bdsp_sourcesText;
	@FindBy(xpath = "//div[@id='mCSB_20_container']/h5")
	public WebElement webelement_bdsp_operattionsText;
	@FindBy(xpath = "//div[@id='mCSB_21_container']/h5")
	public WebElement webelement_bdsp_analityicsText;
	@FindBy(xpath = "//div[@id='mCSB_23_container']/h5")
	public WebElement webelement_bdsp_gisSensitiveText;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[5]/div/div/h5")
	public WebElement webelement_bdsp_iotText;
	@FindBy(xpath = "//div[@id='mCSB_22_container']/h5")
	public WebElement webelement_bdsp_sinksText;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[7]/div/div/h5")
	public WebElement webelement_bdsp_domainText;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[8]/div/div/h5")
	public WebElement webelement_bdsp_industriesText;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[3]/div[1]/div[2]/div/ul/li[9]/div/div/h5")
	public WebElement webelement_bdsp_graphsText;
	@FindBy(xpath = "//*[@id='j_32']")
	public WebElement webelement_bdsp_sourceHDFS;
	@FindBy(xpath = "//*[@id='j_3']")
	public WebElement webelement_bdsp_shinkElasticSearch;
	@FindBy(xpath = "//*[@id='j_136']")
	public WebElement webelement_bdsp_HdfsOnPage;
	@FindBy(xpath = "//*[@id='j_138']")
	public WebElement webelement_bdsp_ElasticOnPage;

	@FindBy(xpath = "//div[@id='indexESsink_chosen']")
	public WebElement webelement_bdsp_esslinkIndex;
	@FindBy(xpath = "//div[@id='indexESsink_chosen']/div/div/input")
	public WebElement webelement_bdsp_esslinkIndexText;
	@FindBy(xpath = "//form[@id='SinkES']/div[5]/div[1]/button")
	public WebElement webelement_bdsp_esslinksubmit;
	@FindBy(xpath = "//form[@id='SinkES']/div[2]")
	public WebElement webelement_bdsp_esFormLoading;
	@FindBy(xpath = "//form[@id='hdfsDataSource']/div[3]/a/span")
	public WebElement webelement_bdsp_hdfsDataSource;
	@FindBy(xpath = "//div[@id='workflow-dashboard']/div[2]")
	public WebElement webelement_bdsploading;
	@FindBy(xpath = "//li[@id='directories_li_0_0']/i[1]")
	public WebElement webelement_bdsp_hdfs_ChoosedataFilesExplore;
	@FindBy(xpath = "//li[@id='directories_li_12 KB_1']")
	public WebElement webelement_bdsp_hdfs_ChoosedataFilesExplore_FirstLink;
	@FindBy(xpath = "//*[@id='/KaabAeroSourceTest/part-00000-f64840b2-66fe-4ca1-ada3-a4bce81ef116.csv#_file']")
	public WebElement webelement_bdsp_hdfs_ChoosedataFilesName;
	@FindBy(xpath = "//button[@id='saveserverDataFiles']")
	public WebElement webelement_bdsp_hdfs_ChoosedataFilesDone;
	@FindBy(xpath = "//div[@id='choosedfilesSource form-group']/div[6]/a/span")
	public WebElement webelement_bdsp_hdfs_defineSchema;
	@FindBy(xpath = "//a[@id='selectAll']")
	public WebElement webelement_bdsp_hdfs_defineSchemaSelectAll;
	@FindBy(xpath = "//div[@id='schemaDataFiles']/div[1]/div/div/div[1]/button[1]")
	public WebElement webelement_bdsp_hdfs_DSDone;
	@FindBy(xpath = "//button[@id='hdfsDataSourceSave']")
	public WebElement webelement_bdsp_hdfs_DSSave;
	@FindBy(xpath = "//button[@id='save-workflow']")
	public WebElement webelement_bdsp_SaveWorkFlow;
	@FindBy(xpath = "//h5[@id='saveInfoText']")
	public WebElement webelement_bdsp_SaveMessage;

	// @FindBy(xpath="")
	// public WebElement webelement_;
	@FindBy(xpath = "")
	public WebElement webelement_;
	// Source Management
	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[12]/button")
	public WebElement webelement_sourceManagement;

	@FindBy(xpath = "//div[@id='source_management_mainDiv']/div[1]/div[1]/ul/li[1]")
	public WebElement webelement_general;
	@FindBy(xpath = "//div[@id='source_management_mainDiv']/div[1]/div[1]/ul/li[2]")
	public WebElement webelement_news;
	@FindBy(xpath = "//div[@id='source_management_mainDiv']/div[1]/div[1]/ul/li[3]")
	public WebElement webelement_index;
	@FindBy(xpath = "//div[@id='source_management_mainDiv']/div[1]/div[1]/div/div[1]/div/div/div/div/div/p")
	public WebElement webelement_totalResorces;
	@FindBy(xpath = "//div[@id='source_management_mainDiv']/div[2]")
	public WebElement webelement_spinner;
	@FindBy(xpath = "//div[1]/div/div/div/div/div/div[1]/div[1]/div[2]/div/input")
	public WebElement webelement_sourceSearch;
	@FindBy(xpath = "//div[1]/div/div/div/div/div/div[1]/div[1]/div[2]/div/button")
	public WebElement webelement_sourceSearchBtn;
	@FindBy(xpath = "//div[contains(@id,'-cell')]/button/i")
	public WebElement webelement_sm_edit;// input[@id="sourceDisplayName"]
	@FindBy(xpath = "//input[@id='sourceDisplayName']")
	public WebElement webelement_sourceDisplayName;
	@FindBy(xpath = "//div[1]/div/div/div/div[3]/form/div/div[4]/button[2]")
	public WebElement webelement_sourceUpdate;
	@FindBy(xpath = "//button[@id='addSource']")
	public WebElement webelement_addSourceBtn;

	@FindBy(xpath = "//input[@id='sourceName']")
	public WebElement webelement_sourceName;

	@FindBy(xpath = "//input[@id='sourceLink']")
	public WebElement webelement_sourceLink;
	@FindBy(xpath = "//div[@id='industryId']/div/button")
	public WebElement webelement_industrySelect;
	@FindBy(xpath = "//div[@id='industryId']/div/ul/li[1]/div/input")
	public WebElement webelement_industrySelectInput;
	@FindBy(xpath = "//div[@id='industryId']/div/ul/li[3]/a/span[2]")
	public WebElement webelement_industrySelectFirst;
	@FindBy(xpath = "//div[@id='jurisdictionId']/div/button")
	public WebElement webelement_jurisdiction;
	@FindBy(xpath = "//div[@id='jurisdictionId']/div/ul/li[1]/div/input")
	public WebElement webelement_jurisdictionInput;
	@FindBy(xpath = "//div[@id='jurisdictionId']/div/ul/li[3]/a/span[2]")
	public WebElement webelement_jurisdictionSelectFirst;
	@FindBy(xpath = "//div[1]/div/div/div/div[2]/form/div/div[2]/div[4]/select")
	public WebElement webelement_categorySelect;
	@FindBy(xpath = "//div[1]/div/div/div/div[2]/form/div/div[4]/button[2]")
	public WebElement webelement_addSource;

	/*
	 * @FindBy(xpath="") public WebElement webelement_;
	 * 
	 * @FindBy(xpath="") public WebElement webelement_;
	 */
	/*
	 * Decision Scoring
	 */
	@FindBy(xpath = "//a[@id='single-button']")
	public WebElement webelement_topLeftMenu;
	@FindBy(xpath = "//div[4]/div[1]/div[2]/ul/li/div/ul/li[5]/a")
	public WebElement webelement_manage;

	@FindBy(xpath = "//div[4]/div[1]/div[2]/ul/li/div/div/div[5]/ul/li[10]/a")
	public WebElement webelement_decisionScoring;

	@FindBy(xpath = "//div[1]/div/div/div[2]/div[1]/div[1]/h3")
	public WebElement webelement_ds_heading;
	@FindBy(xpath = "//div/div[1]/div/div[1]/div/h3[text()='COMPLIANCE']")
	public WebElement webelement_ds_Compliance;
	@FindBy(xpath = "//div[1]/div/div/div[2]/div[1]/div[2]/a/i")
	public WebElement webelement_ds_plusSign;
	@FindBy(xpath = "//flash-message/div/div/span/div")
	public WebElement webelement_ds_newDomainAddedMessage;
	@FindBy(xpath = "//div[@id='mCSB_21_container']/div[7]/div/div[1]/a")
	public WebElement webelement_ds_newDomainToDelete;
	@FindBy(xpath = "//div[@id='mCSB_21_container']/div[7]/div/div[1]/ul/li[3]/a")
	public WebElement webelement_ds_DomainToDelete;
	@FindBy(xpath = "//div[1]/div/div/div[2]/div[2]/div/div/div[1]")
	public WebElement webelement_ds_DeleteConfirmation;
	@FindBy(xpath = "//div[1]/div/div/div[2]/div[1]/div/div/h3/a")
	public WebElement webelement_ds_backtoNewD1;

	@FindBy(xpath = "//p[contains(.,'Decision Scoring')]")
	public WebElement webelement_DecisionScoring;
	// *[@id="mCSB_19_container"]/div[2]/div/div[1]/div/h3
	// @FindBy(xpath="//div[@id='mCSB_20_container']/div[2]/div/div[1]/div/h3")
	@FindBy(xpath = "//h3[contains(.,'NEW D1')]")
	public WebElement webelement_ds_NewD1;
	@FindBy(xpath = "//div[1]/div/div/div[2]/div[1]/div/div/div/div/div/div[1]/div[1]/div/div[2]/strong")
	public WebElement webelement_ds_Entity_Person;
	@FindBy(xpath = "//div[1]/div/div/div[2]/div[1]/div/div/div/div/div/div[1]/div[2]/div/div[2]/strong")
	public WebElement webelement_ds_Entity_Company;

	@FindBy(xpath = "//div[1]/div/div/div[2]/div[1]/div/div/div/div/div/div[2]/div/div[1]/div/div/div")
	public WebElement webelement_ds_aggrigationLevel;

	/**
	 * Policy Enforcement Elements
	 * 
	 */
	// @FindBy(xpath="")
	// public WebElement webelement_;

	@FindBy(xpath = "//p[contains(.,'Policy Enforcement')]")
	public WebElement webelement_policyEnforcement;

	@FindBy(xpath = "//div[@id='home-action-design']/div")
	public WebElement webelement_pe_Design;

	@FindBy(xpath = "//a[contains(text(),'Alertgeneration')]")
	public WebElement webelement_pe_AlertGeneration;

	@FindBy(xpath = "//a[contains(text(),'Alertrules')]") // *[@id="asset-list"]/div[1]/div/div[2]/div/div[1]/a
	public WebElement webelement_pe_AlertRules;

	@FindBy(xpath = "//a[contains(text(),'Source')]")
	public WebElement webelement_pe_Source;
	//// *[@id="_aceGWT0"]/textarea //div[@id="_aceGWT1"]/div[2]/div
	// @FindBy(xpath="//div[2]/div/div/div/div/div[3]/div/div/div/div/div[2]/div")
	// @FindBy(xpath="//div[@id='_aceGWT0']/div[2]/div/div[3]/div[20]")

	@FindBy(xpath = "//div[@id='_aceGWT0']/div[2]/div")
	public WebElement webelement_pe_ReadLineFromSource;

	@FindBy(xpath = "//div[2]/div/div[3]/div[3]")
	public WebElement webelement_pe_sourceEditorLine;

	@FindBy(xpath = "//button[contains(.,'Download')]")
	public WebElement webelement_pe_Downlaod;

	@FindBy(xpath = "//a[contains(text(),'Editor')]")
	public WebElement webelement_pe_Editor;

	@FindBy(xpath = "//div[@id='mega-menu']/nav/div[2]/ul/li/a/i")
	public WebElement webelement_pe_Home;

	@FindBy(xpath = "//a[contains(text(),'servers')]")
	public WebElement webelement_pe_Servers;

	@FindBy(xpath = "//div[@id='status-content']/div/div/div/div/div/div/div/div/p/span/span")
	public WebElement webelement_pe_Server1Status;

	@FindBy(xpath = "//div[@id='status-content']/div/div/div/div/div/div[2]/div/div/p/span/span")
	public WebElement webelement_pe_Server2Status;

	@FindBy(xpath = "//*[@id='container-config-start']") // *[@id="container-config-stop"]
	public WebElement webelement_pe_serverStartStatus;

	/**
	 * Questionnaire Builder Elements
	 */

	@FindBy(xpath = "//button[contains(.,'Questionnaire Builder')]")
	public WebElement webelement_questionnaireBuilder;

	@FindBy(xpath = "//div[@id='panel-2']/div[2]/div")
	public WebElement webelement_listQuestionnaireBuilder;

	@FindBy(xpath = "//tr[6]/td[4]/a")
	public WebElement webelement_question;

	@FindBy(xpath = "//div[4]/div/div[4]/div[3]/div/table/tbody/tr[13]/td[2]")
	public WebElement webelement_noofQuestion;

	@FindBy(xpath = "//button[contains(.,'Preview  KYC Questioner')]")
	public WebElement webelement_previewKYCQuestioner;

	@FindBy(linkText = "English")
	public WebElement webelement_selectLanguage;

	@FindBy(xpath = "//form[@id='limesurvey']/div[4]/div/p") // div[2]/div/div[2]/form/div[4]/div/p
	public WebElement webelement_qusetionsCountText;

	@FindBy(xpath = "//div[@id='group-0']/div/h3")
	public WebElement webelement_qusetionsGroupText;

	@FindBy(xpath = "//textarea")
	public WebElement webelement_firstQuestionAnswer;

	@FindBy(xpath = "//button[@id='movesubmitbtn']")
	public WebElement webelement_questionSubmit;

	@FindBy(xpath = "//div[contains(text(),'Hyderabad')]")
	public WebElement webelement_nativePlaceHyderabad;

	@FindBy(xpath = "//div[2]/div/div[2]/table/tbody/tr/td")
	public WebElement webelement_responseAfterQuestionsubmited;

	@FindBy(xpath = "//div[4]/div/div[2]/div/div/table/tbody/tr[4]/td[3]/a/span")
	public WebElement webelement_qustionActive;

	@FindBy(xpath = "//div[@id='panel-2']/div[2]/div/a/span")
	public WebElement webelement_addQuestion;

	@FindBy(id = "movenextbtn")
	public WebElement webelement_moveNext;

	@FindBy(id = "title")
	public WebElement webelement_title;

	@FindBy(xpath = "//body")
	public WebElement webelement_qBody;

	@FindBy(xpath = "//iframe[@aria-describedby='cke_338']")
	public WebElement webelement_helptextFrame;

	@FindBy(id = "save-button")
	public WebElement webelement_SaveBtn;

	@FindBy(xpath = "//div[4]/div/div[4]/div[2]/form/div[1]/ul/li[2]/a")
	public WebElement webelement_englishLanguageLink;

	@FindBy(xpath = "//div[@id='in_survey_common']/div/div[4]/h3")
	public WebElement webelement_qustionCode;

	@FindBy(xpath = "//div[3]/div[1]/div/div/a[3]/span")
	public WebElement webelement_qustiondelete;

	@FindBy(xpath = "//a[contains(.,' Yes')]")
	public WebElement webelement_qustionDeleteConfirm;

	@FindBy(xpath = "//*[@id='in_survey_common']/div/div[4]/div[2]")
	public WebElement webelement_qustionDeleteConfirmMessage;

	@FindBy(xpath = "//div[4]/div/div[3]/nav/div[3]/ul/li[1]/div/div/ul/li[3]/a/span") // *[@id='dropdown-lvl1']/div/ul/li[3]/a
	public WebElement webelement_listQuestions;

	@FindBy(xpath = "//input[@id='Question_title']")
	public WebElement webelement_searchtextbox;

	@FindBy(xpath = "//input[@name='yt0']")
	public WebElement webelement_searchButton;

	@FindBy(xpath = "//div[@id='question-grid']/table/tbody/tr/td[10]/a[4]/span")
	public WebElement webelement_deleteButton;

	@FindBy(xpath = "//*[@id='survey_title']/a")
	public WebElement webelement_SurveyTitle;

	/*
	 * data curation
	 */ // div[4]/div/div[2]/div[5]/div[2]/div/ul/li[9]/button
	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[9]/button/p")
	public WebElement webelement_dataCuration;
	@FindBy(xpath = "//div[@id='DataTables_Table_1_info']")
	public WebElement webelement_dataTableEntries;
	@FindBy(xpath = "//div[@id='enrich-dashboard']/div/div[2]/button/span")
	public WebElement webelement_dc_Priview;
	@FindBy(xpath = "//input[@id='PreviewName']")
	public WebElement webelement_dc_previewOrgName;
	@FindBy(xpath = "//div[1]/div/div/div[2]/form/div[1]/div[2]/div/input")
	public WebElement webelement_dc_previewPerName;
	@FindBy(xpath = "//div[@id='mCSB_14_container']/tr[2]/td[2]")
	public WebElement webelement_dc_orgID;// *[@id="mCSB_16_container"]/tr[11]/td[2]
	@FindBy(xpath = "//div[@id='mCSB_14_container']/tr[11]/td[2]")
	public WebElement webelement_dc_orgDomicile; // *[@id="mCSB_17_container"]/tr[1]/td[2]
	@FindBy(xpath = "//div[@id='mCSB_15_container']/tr[1]/td[2]")
	public WebElement webelement_dc_perPrefix;
	@FindBy(xpath = "//div[1]/div/div/div[2]/form/div[1]/div[3]/button") // div[1]/div/div/div[2]/form/div[1]/div[3]/button
	public WebElement webelement_dc_search;
	@FindBy(xpath = "//div[1]/div/div/div[1]/button/span")
	public WebElement webelement_dc_close;
	@FindBy(xpath = "//div[1]/div/div/div[2]/form/div[1]/div[1]/label[2]/span")
	public WebElement webelement_dc_person;

	/*
	 * bpm
	 */
	@FindBy(xpath = "//div[4]/div/div[2]/div[5]/div[2]/div/ul/li[1]/button/p")
	public WebElement webelement_orchestration;
	@FindBy(xpath = "//div[@id='kickstart']/div/a/div[1]/h3")
	public WebElement webelement_kickStart;
	@FindBy(xpath = "//h3[contains(.,'Case Management WorkFlow')]") // div[@id='mCSB_1_container']/div[2]/div
	public WebElement webelement_caseManagementWorkFlow;
	@FindBy(xpath = "")
	public WebElement webelement_WorkFlowComponents;

	/**
	 * Policy Enforcement Methods
	 * 
	 * @throws InterruptedException
	 */

	public void clickTopLeftMenu() throws InterruptedException {
		while (!isDisplayed(webelement_topLeftMenu)) {
			Thread.sleep(3000);
			// System.out.println("serverStartStatus is not visible yet");
		}
		webelement_topLeftMenu.click();
	}

	public void clickManage() throws InterruptedException {
		while (!isDisplayed(webelement_manage)) {
			Thread.sleep(3000);
			// System.out.println("serverStartStatus is not visible yet");
		}
		Actions action = new Actions(driver);
		action.moveToElement(webelement_manage).perform();
	}

	public void clickLeftMenuDecisionScoring() throws InterruptedException {
		while (!isDisplayed(webelement_decisionScoring)) {
			Thread.sleep(3000);
			System.out.println("decisionScoring is not visible yet");
		}
		webelement_decisionScoring.click();
	}

	public String getModelDialogHeading() throws InterruptedException {
		while (!isClickable(webelement_ds_heading)) {
			Thread.sleep(3000);
			System.out.println("heading is not visible yet");
		}
		return webelement_ds_heading.getText();
	}

	public String getComlianeDomain() throws InterruptedException {
		while (!isClickable(webelement_ds_Compliance)) {
			Thread.sleep(3000);
			System.out.println("Compliance is not visible yet");
		}
		return webelement_ds_Compliance.getText();
	}

	public void clickPlusSign() throws InterruptedException {
		while (!isDisplayed(webelement_ds_plusSign)) {
			Thread.sleep(3000);
			System.out.println("Plus is not visible yet");
		}
		webelement_ds_plusSign.click();
	}

	public String getNewDomainAddedMessage() throws InterruptedException {
		while (!isDisplayed(webelement_ds_newDomainAddedMessage)) {
			Thread.sleep(3000);
			System.out.println("NewDomainAddedMessage is not visible yet");
		}
		return webelement_ds_newDomainAddedMessage.getText();
	}

	public void clickNewDomaintoDelete() throws InterruptedException {
		while (!isDisplayed(webelement_ds_newDomainToDelete)) {
			Thread.sleep(3000);
			System.out.println("newdomain is not visible yet");
		}
		webelement_ds_newDomainToDelete.click();
	}

	public void clickDomaintoDelete() throws InterruptedException {
		// webelement_ds_DomainToDelete.click();
		try {
			webelement_ds_DomainToDelete.click();
		} catch (org.openqa.selenium.StaleElementReferenceException ex) {
			webelement_ds_DomainToDelete.click();
		}
	}

	public void clickDeleteConfirmation() throws InterruptedException {
		/*
		 * while (!isDisplayed(webelement_ds_DeleteConfirmation)) { Thread.sleep(3000);
		 * System.out.println("DeleteConfirmation is not visible yet"); }
		 * webelement_ds_DeleteConfirmation.click();
		 */
		Actions action = new Actions(driver);
		action.moveToElement(webelement_ds_DeleteConfirmation).click().perform();
		// webelement_ds_DeleteConfirmation.click();
	}

	public void clickBacknewD1() throws InterruptedException {
		Thread.sleep(3000);
		webelement_ds_backtoNewD1.click();
	}

	public void clickPolicyEnforcement() {
		webelement_policyEnforcement.click();
	}

	public void clickDesign() throws InterruptedException {

		while (!isClickable(webelement_pe_Design)) {
			Thread.sleep(3000);
			System.out.println("First Name is not visible yet");
		}
		webelement_pe_Design.click();
	}

	public void clickAlertGeneration() throws InterruptedException {
		while (!isClickable(webelement_pe_AlertGeneration)) {
			Thread.sleep(3000);
			System.out.println("First Name is not visible yet");
		}
		webelement_pe_AlertGeneration.click();
	}

	public void clickAlertRules() throws InterruptedException {
		Thread.sleep(3000);
		while (!isClickable(webelement_pe_AlertRules)) {
			Thread.sleep(3000);
			System.out.println("AlertRules is not visible yet");
		}
		webelement_pe_AlertRules.click();
	}

	public void clickSource() throws InterruptedException {
		while (!isClickable(webelement_pe_Source)) {
			Thread.sleep(3000);
			System.out.println("First Name is not visible yet");
		}
		webelement_pe_Source.click();
	}

	public String getTextFromSource() throws InterruptedException {
		while (!isClickable(webelement_pe_ReadLineFromSource)) {
			Thread.sleep(3000);
			System.out.println("First Name is not visible yet");
		}
		return webelement_pe_ReadLineFromSource.getAttribute("innerText");
	}

	public void clickDownload() throws InterruptedException {
		while (!isClickable(webelement_pe_Downlaod)) {
			Thread.sleep(3000);
			System.out.println("First Name is not visible yet");
		}
		webelement_pe_Downlaod.click();
	}

	public void clickEditor() throws InterruptedException {
		while (!isClickable(webelement_pe_Editor)) {
			Thread.sleep(3000);
			System.out.println("First Name is not visible yet");
		}
		webelement_pe_Editor.click();
	}

	public void clickRandoLine() {
		// webelement_pe_sourceEditorLine.click();
		webelement_pe_sourceEditorLine.sendKeys(Keys.PAGE_DOWN);
	}

	public void clickHome() throws InterruptedException {
		while (!isClickable(webelement_pe_Home)) {
			Thread.sleep(3000);
			System.out.println("First Name is not visible yet");
		}
		webelement_pe_Home.click();
	}

	public void clickServers() throws InterruptedException {
		while (!isClickable(webelement_pe_Servers)) {
			Thread.sleep(3000);
			System.out.println("Servers is not visible yet");
		}
		webelement_pe_Servers.click();
	}

	public void getServer1Status() throws InterruptedException {
		while (!isClickable(webelement_pe_Server1Status)) {
			Thread.sleep(3000);
			System.out.println("Server1Status is not visible yet");
		}
		webelement_pe_Server1Status.getText();
	}

	public void getServer2Status() throws InterruptedException {
		while (!isClickable(webelement_pe_Server2Status)) {
			Thread.sleep(3000);
			System.out.println("Server2Status is not visible yet");
		}
		webelement_pe_Server2Status.getText();
	}

	public String getServerStartStatus() throws InterruptedException {
		while (!isDisplayed(webelement_pe_serverStartStatus)) {
			Thread.sleep(3000);
			// System.out.println("serverStartStatus is not visible yet");
		}
		// System.out.println(webelement_pe_serverStartStatus.getAttribute("class"));
		return webelement_pe_serverStartStatus.getAttribute("class");
	}

	/**
	 * Questionnaire Builder Methods
	 */
	public void clickQuestionnaireBuilder() {
		webelement_questionnaireBuilder.click();
	}

	public void clickListQuestionnaireBuilder() {
		webelement_listQuestionnaireBuilder.click();
	}

	public void clickOnPerticularQuestion() {
		webelement_question.click();
	}

	public String getNoOfQuestions() {
		return webelement_noofQuestion.getText();
	}

	public void clickPreviewKYCQuestioner() {
		webelement_previewKYCQuestioner.click();
	}

	public void clickLanguage() {
		webelement_selectLanguage.click();
	}

	public String getNoofQuestionsText() {
		return webelement_qusetionsCountText.getText();
	}

	public String getFirstGroupName() {
		return webelement_qusetionsGroupText.getText();
	}

	public void typeFirstQuestionAnswer(String fanswer) {
		webelement_firstQuestionAnswer.sendKeys(fanswer);
	}

	public void clickSubmit() {
		webelement_questionSubmit.click();
	}

	public void clickHyderabad() {
		webelement_nativePlaceHyderabad.click();
	}

	public String getQuestionSubmittedResultText() {
		return webelement_responseAfterQuestionsubmited.getText();
	}

	public String getStateofTheQuestion() {
		System.out.println("--" + webelement_qustionActive.getAttribute("class"));
		return webelement_qustionActive.getAttribute("class");
	}

	public void clickAddQuastion() {
		webelement_addQuestion.click();
	}

	public void clickMoveNext() {
		webelement_moveNext.click();
	}

	public void enterTitle() {
		webelement_title.sendKeys(util.getQuestionTitle());
	}

	public String getTitle() {
		return webelement_title.getText();
	}

	public void enterQBody() {
		webelement_qBody.sendKeys("what is your qualification??");
	}

	public void enterQHelp() {
		webelement_qBody.sendKeys("Help text here");
	}

	public void clickSaveBtn() throws InterruptedException {
		while (!isClickable(webelement_SaveBtn)) {
			Thread.sleep(3000);
			System.out.println("Save btn is not visible yet");
		}
		webelement_SaveBtn.click();
	}

	public void clickEnglishLanguage() {
		webelement_englishLanguageLink.click();
	}

	public void tabToNextField() {
		webelement_englishLanguageLink.sendKeys(Keys.TAB);
	}

	public String getQuestionCodeText() {
		System.out.println("---");
		return webelement_qustionCode.getText();
	}

	public void clickDeleteQeustion() {
		webelement_qustiondelete.click();
	}

	public void clickDeleteQuestionConformation() {
		driver.findElement(By.xpath("//*[@id=\"confirmation-modal\"]/div/div/div[3]/a")).click();
	}

	public String getDeleteSuccessMessage() {
		return driver.findElement(By.xpath("//*[@class='modal-body']/p")).getText();
	}

	public void clickListQuestions() {
		webelement_listQuestions.click();
	}

	public void typeSearchtext() {
		webelement_searchtextbox.sendKeys(util.getQuestionTitle());
	}

	public String questionTitle() {
		System.out.println("==");
		return util.getQuestionTitle();
	}

	public void clickSearchBtn() {
		webelement_searchButton.click();
	}

	public void clickDeleteQuastion() {
		webelement_deleteButton.click();
	}

	public void clickSurveyTitle() {
		webelement_SurveyTitle.click();
	}

	// Decision Scoring
	public void clickDecisionScoring() {
		webelement_DecisionScoring.click();
	}

	public void clickNewD1() throws InterruptedException {
		while (!isClickable(webelement_ds_NewD1)) {
			Thread.sleep(3000);
			System.out.println("New D1 is not visible yet");
		}
		webelement_ds_NewD1.click();
	}

	public String getPersonDefaultScore() {
		return webelement_ds_Entity_Person.getText();
	}

	public String getCompanyDefaultScore() {
		return webelement_ds_Entity_Company.getText();
	}

	public int getAggrigationLevel() {
		return driver
				.findElements(
						By.xpath("//div[1]/div/div/div[2]/div[1]/div/div/div/div/div/div[2]/div/div[1]/div/div/div/p"))
				.size();

	}

	// data curation
	public void clickDataCuration() {
		webelement_dataCuration.click();
	}

	public String getDataTableEntriesCountText() {
		return webelement_dataTableEntries.getText();
	}

	public void clickPreview() throws InterruptedException {
		while (!isDisplayed(webelement_dc_Priview)) {
			Thread.sleep(3000);
			System.out.println("previewOrg Name is not visible yet");
		}
		webelement_dc_Priview.click();
	}

	public void enterOrgName(String string) throws InterruptedException {
		while (!isDisplayed(webelement_dc_previewOrgName)) {
			Thread.sleep(3000);
			System.out.println("previewOrg Name is not visible yet");
		}
		webelement_dc_previewOrgName.clear();
		webelement_dc_previewOrgName.sendKeys(string);
		// webelement_dc_previewOrgName.sendKeys(Keys.ARROW_DOWN);
		// webelement_dc_previewOrgName.sendKeys(Keys.ENTER);
	}

	public void enterPersonName(String string) throws InterruptedException {
		while (!isDisplayed(webelement_dc_previewPerName)) {
			Thread.sleep(3000);
			System.out.println("previewPerName is not visible yet");
		}
		webelement_dc_previewPerName.clear();
		webelement_dc_previewPerName.sendKeys(string);
		// webelement_dc_previewPerName.sendKeys(Keys.ARROW_DOWN);
		// webelement_dc_previewPerName.sendKeys(Keys.ENTER);
	}

	public String orgid() throws InterruptedException {
		/*
		 * while (!isDisplayed(webelement_dc_orgID)) { for(int i=0;i<10;i++) {
		 * Thread.sleep(3000); System.out.println("orgID is not visible yet");
		 * 
		 * } break; }
		 */
		return webelement_dc_orgID.getText();
	}

	public String orgDomicile() throws InterruptedException {

		return webelement_dc_orgDomicile.getText();
	}

	public String perPrefix() {
		return webelement_dc_perPrefix.getText();
	}

	public void clickSearch() {
		webelement_dc_search.click();
	}

	public void clickClose() throws InterruptedException {
		while (!isDisplayed(webelement_dc_close)) {
			Thread.sleep(3000);
			System.out.println("clickPerson is not visible yet");
		}
		webelement_dc_close.click();
	}

	public void clickPerson() throws InterruptedException {
		while (!isDisplayed(webelement_dc_person)) {
			Thread.sleep(3000);
			System.out.println("clickPerson is not visible yet");
		}
		webelement_dc_person.click();
	}

	public static boolean isDisplayed(WebElement element) {
		try {
			if (element.isDisplayed())
				return element.isDisplayed();
		} catch (NoSuchElementException ex) {
			return false;
		}
		return false;
	}

	public static boolean isClickable(WebElement element) {
		try {
			if (element.isEnabled())
				return element.isEnabled();
		} catch (NoSuchElementException ex) {
			return false;
		}
		return false;
	}

	public void switchingWindow(int i) throws InterruptedException {
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		System.out.println("size is : " + tabs2.size());
		driver.switchTo().window(tabs2.get(i));
		Thread.sleep(25000);
	}

	public void switchingToHelpTextFrame() {

		driver.switchTo().defaultContent();
		driver.switchTo().frame(webelement_helptextFrame);
	}

	public void clickSourceManagement() throws InterruptedException {
		while (!isDisplayed(webelement_sourceManagement)) {
			Thread.sleep(3000);
			System.out.println("sourceManagement is not visible yet");
		}
		webelement_sourceManagement.click();
	}

	public void clickSMGeneral() throws InterruptedException {
		while (!isDisplayed(webelement_general)) {
			Thread.sleep(3000);
			System.out.println("sourceManagement is not visible yet");
		}
		webelement_general.click();
	}

	public boolean getSMGeneralActive() {
		return webelement_general.getAttribute("class").contains("active");
	}

	public void clickSMNews() throws InterruptedException {
		while (!isDisplayed(webelement_news)) {
			Thread.sleep(3000);
			System.out.println("sourceManagement is not visible yet");
		}
		webelement_news.click();
	}

	public void clickSMIndex() throws InterruptedException {
		while (!isDisplayed(webelement_index)) {
			Thread.sleep(3000);
			System.out.println("sourceManagement is not visible yet");
		}
		webelement_index.click();
	}

	public String getSMResources() throws InterruptedException {
		getSpinnerloading();
		String text = webelement_totalResorces.getAttribute("innerText")
				.replace("Prioritize information from a source for each category", "");
		text = text.replace("(", "").replace(")", "").replace("sources", "").replace(".", "").replace(" ", "");
		text = text.replace("\n", "").replace("										", "");
		return text;
	}

	public void getSpinnerloading() throws InterruptedException {
		while (!webelement_spinner.getAttribute("class").contains("ng-hide")) {
			Thread.sleep(1000);
			System.out.println("Loading please wait");
		}
	}

	public void typeSource() {
		webelement_sourceSearch.sendKeys("walmart");
		webelement_sourceSearchBtn.click();
	}

	public boolean searchSourceText() throws InterruptedException {
		getSpinnerloading();
		List<WebElement> we = driver
				.findElements(By.xpath("//div[contains(@id,'body-grid-container')]/div[2]/div/div"));
		System.out.println("total walmart findings are " + we.size());
		boolean prasent = false;
		for (int i = 1; i <= we.size(); i++) {
			System.out.println("///" + driver
					.findElement(By.xpath(
							"//div[contains(@id,'body-grid-container')]/div[2]/div/div[" + i + "]/div/div/div/p"))
					.getText());
			if (driver
					.findElement(By.xpath(
							"//div[contains(@id,'body-grid-container')]/div[2]/div/div[" + i + "]/div/div/div/p"))
					.getText().toLowerCase().contains("walmart")) {
				// System.out.println("if yes");
				prasent = true;
			} else {
				// System.out.println("else no");
				prasent = false;
				break;
			}
		}
		return prasent;
	}

	public void clickedit() throws InterruptedException {

		driver.findElement(By.xpath("//div[contains(@id,'body-grid-container')]/div[2]/div/div")).click();
		driver.findElement(By.xpath("//div[contains(@id,'body-grid-container')]/div[2]/div/div")).click();
		driver.findElement(By.xpath("//div[contains(@id,'body-grid-container')]/div[2]/div/div")).click();
		driver.findElement(By.xpath("//div[contains(@id,'body-grid-container')]/div[2]/div/div")).click();
		driver.findElement(By.xpath("//div[contains(@id,'body-grid-container')]/div[2]/div/div")).click();
		driver.findElement(By.xpath("//div[contains(@id,'body-grid-container')]/div[2]/div/div")).click();
		driver.findElement(By.xpath("//div[contains(@id,'body-grid-container')]/div[2]/div/div")).click();
		// driver.findElement(By.xpath("//div[contains(@id,'body-grid-container')]/div[2]/div/div")).click();
		System.out.println("element clicked fine");
		webelement_sm_edit.click();// div[contains(@id,'-cell')]/button/i

	}

	public void enterSourceName() throws InterruptedException {
		while (!isDisplayed(webelement_sourceDisplayName)) {
			Thread.sleep(3000);
			System.out.println("sourceDisplayName is not visible yet");
		}
		webelement_sourceDisplayName.clear();
		webelement_sourceDisplayName.sendKeys("walmart5");
	}

	public void clicksourceUpdate() {

		webelement_sourceUpdate.click();
	}

	public void clickAddSourceBtn() throws InterruptedException {
		while (!isDisplayed(webelement_addSourceBtn)) {
			Thread.sleep(3000);
			System.out.println("addSourceBtn is not visible yet");
		}
		webelement_addSourceBtn.click();
	}

	public void typeSourceName() {
		webelement_sourceName.sendKeys("walmart2");
	}

	public void typeSourceLink() {
		webelement_sourceLink.sendKeys("www.walmart.com");
	}

	public void clickIndustrySelect() {
		webelement_industrySelect.click();
	}

	public void typeIndustrySelect() {
		webelement_industrySelectInput.sendKeys("Communications");
	}

	public void clickIndustrySelectflink() {
		webelement_industrySelectFirst.click();
	}

	public void clickJurisdictionSelect() {
		webelement_jurisdiction.click();
	}

	public void typeJurisdictionSelect() {
		webelement_jurisdictionInput.sendKeys("United State");
	}

	public void clickJurisdictionSelectFLink() {
		webelement_jurisdictionSelectFirst.click();
	}

	public void selectCategory() {
		Select category = new Select(webelement_categorySelect);
		category.selectByValue("Company Register");
	}

	public void clickAddSource() {
		webelement_addSource.click();
	}

	public void clickBDSP() throws InterruptedException {
		while (!isDisplayed(webelement_bdsp)) {
			Thread.sleep(3000);
			System.out.println("BDSP is not visible yet");
		}
		webelement_bdsp.click();
	}

	public void clickcreateNewWorkflow() throws InterruptedException {
		Thread.sleep(3000);
		while (!isDisplayed(webelement_createNewWorkflow)) {
			Thread.sleep(3000);
			System.out.println("createNewWorkflow is not visible yet");
		}
		webelement_createNewWorkflow.click();
	}

	public void typeWorkFlowName() {
		webelement_workflowName.clear();
		webelement_workflowName.sendKeys("test_workflow");
	}

	public void clickCreate() {
		webelement_workflowcreatenew.click();
	}

	public String getBDSPSource() {
		Actions action = new Actions(driver);
		action.moveToElement(webelement_bdsp_sources).perform();
		return webelement_bdsp_sourcesText.getText();
	}

	public String getBDSPOperations() {
		Actions action = new Actions(driver);
		action.moveToElement(webelement_bdsp_operations).perform();
		return webelement_bdsp_operattionsText.getText();
	}

	public String getBDSPAnalytic() {
		Actions action = new Actions(driver);
		action.moveToElement(webelement_bdsp_analytic).perform();
		return webelement_bdsp_analityicsText.getText();
	}

	public String getBDSPGisSensivity() {
		Actions action = new Actions(driver);
		action.moveToElement(webelement_bdsp_gissensivity).perform();
		return webelement_bdsp_gisSensitiveText.getText();
	}

	public String getBDSPiot() {
		Actions action = new Actions(driver);
		action.moveToElement(webelement_bdsp_iot).perform();
		return webelement_bdsp_iotText.getText();
	}

	public String getBDSPSinks() {
		Actions action = new Actions(driver);
		action.moveToElement(webelement_bdsp_sinks).perform();
		return webelement_bdsp_sinksText.getText();
	}

	public String getBDSPDomain() {
		Actions action = new Actions(driver);
		action.moveToElement(webelement_bdsp_domain).perform();
		return webelement_bdsp_domainText.getText();
	}

	public String getBDSPIndustries() {
		Actions action = new Actions(driver);
		action.moveToElement(webelement_bdsp_industries).perform();
		return webelement_bdsp_industriesText.getText();
	}

	public String getBDSPGraphs() {
		Actions action = new Actions(driver);
		action.moveToElement(webelement_bdsp_graphs).perform();
		return webelement_bdsp_graphsText.getText();
	}

	public int bdspRightPanelicons() {
		return driver.findElements(By.xpath("//div[@id='workflow-dashboard']/div[3]/div[1]/div[3]/div/ul/li")).size();
	}

	public void getBDSPSourcehdfs() {
		Actions action = new Actions(driver);
		action.moveToElement(webelement_bdsp_sources).perform();
		webelement_bdsp_sourceHDFS.click();
	}

	public void getBDSPSinksElasticSearch() {
		Actions action = new Actions(driver);
		action.moveToElement(webelement_bdsp_sinks).perform();
		webelement_bdsp_shinkElasticSearch.click();
	}

	public void clickHdfs() throws InterruptedException {
		Thread.sleep(3000);
		while (!isDisplayed(webelement_bdsp_HdfsOnPage)) {
			Thread.sleep(3000);
			System.out.println("esslinkIndex is not visible yet");
		}
		Actions action = new Actions(driver);
		action.doubleClick(webelement_bdsp_HdfsOnPage).perform();
	}

	public void clickElastic() throws InterruptedException {
		while (!isDisplayed(webelement_bdsp_ElasticOnPage)) {
			Thread.sleep(3000);
			System.out.println("esslinkIndex is not visible yet");
		}
		Actions action = new Actions(driver);
		// action.moveToElement(webelement_bdsp_ElasticOnPage).perform();
		action.doubleClick(webelement_bdsp_ElasticOnPage).perform();
		// webelement_bdsp_ElasticOnPage.click();
		// webelement_bdsp_ElasticOnPage.click();
	}

	public void clickonIndex() throws InterruptedException {
		while (!isDisplayed(webelement_bdsp_esslinkIndex)) {
			Thread.sleep(3000);
			System.out.println("esslinkIndex is not visible yet");
		}
		webelement_bdsp_esslinkIndex.click();
	}

	public void typeIndexName() throws InterruptedException {
		getLoading();
		while (!isDisplayed(webelement_bdsp_esslinkIndexText)) {
			Thread.sleep(3000);
			System.out.println("esslinkIndexText is not visible yet");
		}
		webelement_bdsp_esslinkIndexText.sendKeys("persons");
		webelement_bdsp_esslinkIndexText.sendKeys(Keys.ARROW_DOWN);
		webelement_bdsp_esslinkIndexText.sendKeys(Keys.ENTER);
	}

	public void clickSave() throws InterruptedException {
		getLoading();
		webelement_bdsp_esslinksubmit.click();
	}

	public void getLoading() throws InterruptedException {
		while (!webelement_bdsp_esFormLoading.getAttribute("style").contains("display: none;")) {
			Thread.sleep(1000);
			System.out.println("Loading please wait");
		}

	}

	public void clcikhdfsDataSource() throws InterruptedException {
		while (!isDisplayed(webelement_bdsp_hdfsDataSource)) {
			Thread.sleep(3000);
			System.out.println("hdfsDataSource is not visible yet");
		}
		webelement_bdsp_hdfsDataSource.click();
	}

	public void clcikhdfsChoosedataFilesExplore() throws InterruptedException {
		loading();
		webelement_bdsp_hdfs_ChoosedataFilesExplore.click();
	}

	public void loading() throws InterruptedException {
		while (!webelement_bdsploading.getAttribute("style").contains("display: none;")) {
			Thread.sleep(1000);
			System.out.println("Loading please wait");
		}
	}

	public void clickhdfsCDFExploreFirstLink() {
		webelement_bdsp_hdfs_ChoosedataFilesExplore_FirstLink.click();
	}

	public void clickhdfsCDFName() {
		webelement_bdsp_hdfs_ChoosedataFilesName.click();
	}

	public void clickhdfsCDFDone() {
		webelement_bdsp_hdfs_ChoosedataFilesDone.click();
	}

	public void clickhdfsDefineSchema() {
		webelement_bdsp_hdfs_defineSchema.click();
	}

	public void clickhdfsDSSelectAll() throws InterruptedException {
		loading();
		webelement_bdsp_hdfs_defineSchemaSelectAll.click();
	}

	public void clickhdfsDSDone() {
		webelement_bdsp_hdfs_DSDone.click();
	}

	public void clickhdfsDSSave() {
		webelement_bdsp_hdfs_DSSave.click();
	}

	public void clickSaveWorkflow() {
		webelement_bdsp_SaveWorkFlow.click();
	}

	public String getBDSPSAveSuccessMessage() {
		return webelement_bdsp_SaveMessage.getText();
	}

	public void dragConnection() throws InterruptedException, AWTException {
		System.out.println("hdfs" + webelement_bdsp_HdfsOnPage.getLocation());
		System.out.println("elastic" + webelement_bdsp_ElasticOnPage.getLocation());
		/*
		 * Actions builder=new Actions(driver); Action dragAndDrop =
		 * builder.clickAndHold(webelement_bdsp_HdfsOnPage).moveToElement(
		 * webelement_bdsp_ElasticOnPage).release(webelement_bdsp_ElasticOnPage).build()
		 * ; Thread.sleep(2000); dragAndDrop.perform();
		 */

	}

	public void clickOrchestration() {
		webelement_orchestration.click();
	}

	public void clickkickStart() {
		webelement_kickStart.click();
	}

	public void clickCaseManagement() {
		webelement_caseManagementWorkFlow.click();
	}

	public int getComponentCount() {
		// return driver.findElements(By.cssSelector("rect:nth-child(25)")).size();
		WebElement svg = driver.findElement(By.cssSelector("svg"));
		List<WebElement> outertext = svg.findElements(By.cssSelector("rect"));

		return outertext.size();
		/*
		 * for(WebElement texts : outertext) { String textcollection = texts.getText();
		 * if(textcollection.equals("xxxxxx")) { texts.click(); } }
		 */
	}

}
