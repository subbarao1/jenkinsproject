package com.element.admin.integration.manage;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.openqa.selenium.WebDriver;
import com.element.admin.integration.manage.ManagePage;
import com.element.admin.integration.aml.AmlPage;
import com.element.admin.integration.common.CommonTest;
import com.element.admin.integration.common.Login;
import com.element.admin.integration.common.Util;
import com.element.admin.integration.shared.SharedPage;

public class DecisionScoringTest extends CommonTest {

	WebDriver driver;
	String actual = "", expected = "";
	Util util = new Util();

	@BeforeClass
	public void startup() {
		final Login login = new Login(driver);
		driver = login.setup();
	}

	@Test(priority = 1)
	public void domain() {
		final AmlPage aml = new AmlPage(driver);
		aml.clickAML();
	}

	@Test(priority = 2)
	public void process() {
		final SharedPage spage = new SharedPage(driver);
		spage.clickAdvanceSearch();
	}

	@Test(priority = 3)
	public void TS_001_09() throws InterruptedException {
		final ManagePage manage = new ManagePage(driver);
		manage.clickTopLeftMenu();
		manage.clickManage();

		manage.clickLeftMenuDecisionScoring();
		actual = manage.getModelDialogHeading();
		expected = "PLEASE SELECT A DOMAIN";
		AssertJUnit.assertEquals(actual, expected);
		System.out.println("Model is opened with the heading 'Please select a domain'");
		actual = manage.getComlianeDomain();
		expected = "COMPLIANCE";
		AssertJUnit.assertEquals(actual, expected);
		System.out.println("Model contained a 'Complaince' domain");
		manage.clickPlusSign();
		actual = manage.getNewDomainAddedMessage();
		expected = "DOMAIN CREATED SUCCESSFULLY";
		AssertJUnit.assertTrue(actual.contains(expected));
		System.out.println("+ button is at a bottom right  as expected");
		Thread.sleep(4000);
		manage.clickNewD1();
		System.out.println("total Domains are : " + manage.getAggrigationLevel());
		System.out.println("TS_001_09 passed");
	}

	@Test(priority = 4)
	public void TS_001_10() throws InterruptedException {
		final ManagePage manage = new ManagePage(driver);
		expected = "0.33";
		actual = manage.getCompanyDefaultScore();
		AssertJUnit.assertEquals(actual, expected);
		expected = "0.52";
		actual = manage.getPersonDefaultScore();
		AssertJUnit.assertEquals(actual, expected);
		expected = "6";
		actual = String.valueOf(manage.getAggrigationLevel());
		AssertJUnit.assertEquals(actual, expected);
		manage.clickBacknewD1();
		System.out.println("TS_001_10 passed");
	}

	@Test(priority = 5)
	public void deleteDomain() throws InterruptedException {
		final ManagePage manage = new ManagePage(driver);

		Thread.sleep(5000);
		manage.clickNewDomaintoDelete();
		manage.clickDomaintoDelete();
		Thread.sleep(5000);
		manage.clickDeleteConfirmation();
	}

	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}
}
