package com.element.admin.integration.manage;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import com.element.admin.integration.aml.AmlPage;
import com.element.admin.integration.common.Login;

import org.openqa.selenium.WebDriver;

public class DataCurationTest {
	WebDriver driver;
	String actual = "", expected = "";

	@BeforeClass
	public void startup() {
		final Login login = new Login(driver);
		driver = login.setup();
	}

	@Test(priority = 1)
	public void domain() {
		final AmlPage aml = new AmlPage(driver);
		aml.clickAML();
	}

	@Test(priority = 2)
	public void TS_001_20() throws InterruptedException {
		final ManagePage manage = new ManagePage(driver);
		manage.clickDataCuration();
		expected = "Showing 1 to 10 of 10 entries";
		actual = manage.getDataTableEntriesCountText();
		// System.out.println("total count findings"+
		// manage.getDataTableEntriesCountText());
		AssertJUnit.assertEquals(actual, expected);
	}

	@Test(priority = 3)
	public void TS_001_21() throws InterruptedException {
		final ManagePage manage = new ManagePage(driver);
		manage.clickPreview();
		manage.enterOrgName("DIXONS CARPHONE PLC");
		manage.clickSearch();
		try {
			expected = "07105905";
			actual = manage.orgid();
			System.out.println(actual);
			AssertJUnit.assertEquals(expected, actual);

			expected = "GB";
			actual = manage.orgDomicile();
			System.out.println(actual);
			AssertJUnit.assertEquals(expected, actual);
		} catch (Exception e) {
			System.out.println("not found");
		} finally {
			manage.clickClose();
		}

	}

	@Test(priority = 4)
	public void TS_001_22() throws InterruptedException {
		final ManagePage manage = new ManagePage(driver);
		manage.clickPreview();
		manage.clickPerson();
		manage.enterPersonName("Michael Mindel");
		manage.clickSearch();

		expected = "Mr";
		actual = manage.perPrefix();
		System.out.println(actual);
		AssertJUnit.assertEquals(expected, actual);
	}

	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}
}
