package com.element.admin.integration.manage;

import org.testng.annotations.Test;

import com.element.admin.integration.aml.AmlPage;
import com.element.admin.integration.common.Login;
import com.element.admin.integration.common.Util;

import org.testng.annotations.BeforeClass;
//import org.testng.AssertJUnit;
import java.util.ArrayList;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class QuestionnaireBuilderTest {
	WebDriver driver;
	Util util;
	String companyname;
	String jurisdiction;
	String actual = null, expected = null;
	String temp = null;
	ManagePage manage;

	@BeforeClass
	public void startup() {
		final Login login = new Login(driver);
		driver = login.setup();
	}

	@Test(priority = 2)
	public void domain() {
		final AmlPage aml = new AmlPage(driver);
		aml.clickAML();
	}

	@Test(priority = 3)
	public void manage() {
		manage = new ManagePage(driver);
		manage.clickQuestionnaireBuilder();

	}

	/*
	 * public void clearValues() { actual = ""; expected = ""; }
	 */
	@Test(priority = 4)
	public void TS_001_15() throws InterruptedException {
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		System.out.println("size is : " + tabs2.size());
		driver.switchTo().window(tabs2.get(1));
		// Thread.sleep(10000);
		manage.clickListQuestionnaireBuilder();
		temp = manage.getStateofTheQuestion();
		manage.clickOnPerticularQuestion();
		String questions = manage.getNoOfQuestions();
		String[] num = questions.split("/");
		expected = num[0];
		// System.out.println("No of Questions "+questions);
		manage.clickPreviewKYCQuestioner();
		manage.clickLanguage();
	}

	@Test(priority = 5)
	public void questionNumberCompare() throws InterruptedException {
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		System.out.println("size is : " + tabs2.size());
		driver.switchTo().window(tabs2.get(2));
		// Thread.sleep(10000);
		// clearValues();
		actual = manage.getNoofQuestionsText();
		// System.out.println(actual);

		System.out.println(actual);
		actual = actual.replaceAll("[^.-?0-9]+", " ").replaceAll(" ", "").replace(".", "");
		System.out.println("question number compare : " + actual);
		// actual=actual.substring(10, 12);
		// System.out.println(actual);

		Assert.assertEquals(actual, expected);
	}

	@Test(priority = 6)
	public void getFirstQuestionGroupName() {
		manage.clickMoveNext();
		// clearValues();
		actual = manage.getFirstGroupName();
		System.out.println("Actual is : " + actual);
		expected = "My first question group";
		Assert.assertEquals(actual, expected);
	}

	@Test(priority = 7)
	public void TS_001_16() {
		manage.typeFirstQuestionAnswer("This is the answer for the first question");
		// manage.clickHyderabad();
		// clearValues();
		manage.clickMoveNext();
		manage.clickMoveNext();
		manage.clickSubmit();
		actual = manage.getQuestionSubmittedResultText();
		// System.out.println(",,,"+actual);
		// System.out.println("..."+temp);
		if (temp.contains("stop")) {
			Assert.assertTrue(
					actual.contains("Your survey responses have not been recorded. This survey is not yet active."));
		}
	}

	@Test(priority = 8)
	public void TS_001_17() throws InterruptedException {
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		System.out.println("size is : " + tabs2.size());
		driver.switchTo().window(tabs2.get(1));
		manage.clickAddQuastion();
		manage.enterTitle();
		manage.clickEnglishLanguage();
		Thread.sleep(5000);
		manage.tabToNextField();
		driver.switchTo().defaultContent();
		driver.switchTo().frame(2);
		manage.enterQBody();
		driver.switchTo().defaultContent();
		driver.switchTo().frame(3);
		// manage.switchingToHelpTextFrame();
		manage.enterQHelp();
		driver.switchTo().defaultContent();
		manage.clickSaveBtn();

	}

	@Test(priority = 9)
	public void addQuestionResult() {
		System.out.println("test add question");
		// clearValues();
		actual = manage.questionTitle();
		System.out.println("actual is: " + actual);
		expected = manage.getQuestionCodeText();

		System.out.println("expec is " + expected);
		Assert.assertTrue(expected.contains(actual));

	}

	@Test(priority = 10)
	public void deleteQuestion() throws InterruptedException {
		Thread.sleep(5000);
		manage.clickSurveyTitle();
		manage.clickListQuestions();
		manage.typeSearchtext();
		manage.clickSearchBtn();
		manage.clickDeleteQuastion();
		Alert alert = driver.switchTo().alert();
		alert.accept();

	}

	@AfterClass
	public void tearDown() {
		// driver.close();
		// driver.quit();
	}

}
