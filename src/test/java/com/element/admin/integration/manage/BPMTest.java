package com.element.admin.integration.manage;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.openqa.selenium.WebDriver;
import com.element.admin.integration.manage.ManagePage;
import com.element.admin.integration.aml.AmlPage;
import com.element.admin.integration.common.Login;

public class BPMTest {
	WebDriver driver;
	String actual = "", expected = "";

	@BeforeClass
	public void startup() {
		final Login login = new Login(driver);
		driver = login.setup();
	}

	@Test(priority = 1)
	public void domain() {
		final AmlPage aml = new AmlPage(driver);
		aml.clickAML();
	}

	@Test(priority = 2)
	public void TS_001_07() throws InterruptedException {
		final ManagePage manage = new ManagePage(driver);
		manage.clickOrchestration();
		manage.clickkickStart();
		manage.clickCaseManagement();
		System.out.println("Components count are : " + manage.getComponentCount());
		AssertJUnit.assertTrue(manage.getComponentCount() > 10);
	}

	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}
}
