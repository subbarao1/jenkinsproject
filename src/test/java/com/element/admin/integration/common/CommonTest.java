package com.element.admin.integration.common;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.io.FileHandler;
import org.testng.Assert;

public class CommonTest {

	public static String getScreenShot(WebDriver driver) {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File src = ts.getScreenshotAs(OutputType.FILE);
		String path = System.getProperty("user.dir") + "/ScreenShot/" + System.currentTimeMillis();
		File destination = new File(path);
		try {
			FileHandler.copy(src, destination);
		} catch (Exception e) {

			System.out.println("captured failes" + e.getMessage());
		}

		return path;
	}

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"/home/ahextech/subbarao/Resources/chromedriver_linux64/chromedriver");
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("http://www.google.com");
			Assert.assertEquals("yes", "no");
		} catch (Exception e) {
			System.out.println("czxcz" + getScreenShot(driver));
		}
	}
}
