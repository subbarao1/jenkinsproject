package com.element.admin.integration.common;

import java.io.IOException;

//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.element.admin.integration.common.Util;

public class Login {
	public WebDriver driver;
	String url;
	Util util;
	String username;
	String password;
	String companyname;
	String jurisdiction;
	String actual = null, expected = null;
	String temp = null;

	public int DRIVER_WAIT = 30; // 30 seconds

	/**
	 * Constructor.
	 *
	 * @param driver an instance of WebDriver
	 */
	public Login(final WebDriver driver) {
		final ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, DRIVER_WAIT);
		PageFactory.initElements(finder, this);
		this.driver = driver;
	}

	@FindBy(xpath = "//input[@id='username']")
	public WebElement webelement_username;

	@FindBy(xpath = "//input[@id='password']")
	public WebElement webelement_password;

	@FindBy(xpath = "//button[@id='signIn']")
	public WebElement webelement_loginbutton;

	public void login(String username, String password) {
		webelement_username.click();
		webelement_username.sendKeys(username);
		webelement_password.sendKeys(password);
		webelement_loginbutton.click();
	}

	public void waitforElement() {
		(new WebDriverWait(driver, 1000))
				.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[text()='LOADING...']")));
		int num = driver.findElements(By.xpath("//*[text()='DATA NOT FOUND']")).size();
		System.out.println("The Number is : " + num);
		if (num > 2)
			Assert.fail();
	}

	public WebDriver setup() {
		util = new Util();

		username = util.getUIUsername();
		password = util.getUIPassword();
		companyname = util.getCompanyName();
		// System.out.println("|||"+companyname);
		jurisdiction = util.getJurisdiction();
		url = util.getServerUrl();

		final String browser = util.getBrowser();

		if (browser.equalsIgnoreCase("Firefox")) {
			driver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("IE")) {
			driver = new InternetExplorerDriver();
		} else if (browser.equalsIgnoreCase("Chrome")) {
			System.setProperty(util.getDriverName(), util.getDriverPath());
			driver = new ChromeDriver();
		} else {
			driver = new FirefoxDriver();
		}

		driver.get(url);
		driver.manage().window().maximize();
		final Login login = new Login(driver);
		login.login(username, password);

		return driver;
	}

	public void readExcel() throws IOException {

	}

}
