package com.element.admin.integration.common;

import java.io.IOException;
import java.io.InputStream;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

/**
 * DOCUMENT ME!
 *
 * @version $Revision$, $Date$
 */
public class Util {
	String browser = "IE";
	String server_url = "";
	String ui_password = "";
	String ui_username = "";
	String companyname = "";
	String jurisdiction = "";
	String qTitle = "";
	String filePath = "";
	String fileName = "";
	String driverpath = "";
	String drivername = "";
	String download = "";
	String file_name = "";

	private final Properties properties = new Properties();

	/**
	 * Creates a new Util object.
	 */
	public Util() {
		loadConfig();

	}

	/**
	 * Returns the browser value.
	 *
	 * @return browser value.
	 */
	public String getBrowser() {
		return browser;
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param key
	 *
	 * @return Properties
	 */
	public String getMessage(final String key) {
		return (String) properties.get(key);
	}

	/**
	 * Returns the UIPassword value.
	 *
	 * @return UIPassword value.
	 */
	public String getUIPassword() {
		return ui_password;
	}

	/**
	 * Returns the UIUsername value.
	 *
	 * @return UIUsername value.
	 */
	public String getUIUsername() {
		return ui_username;
	}

	public String getCompanyName() {
		return companyname;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	/**
	 * DOCUMENT ME!
	 */
	public void loadConfig() {
		final Properties connProps = new Properties();

		try {
			final InputStream in = Util.class.getResourceAsStream("/connection.properties");
			connProps.load(in);

			if (connProps != null) {
				browser = connProps.getProperty("testbrowser");
				server_url = connProps.getProperty("app.url");
				ui_username = connProps.getProperty("ui.username");
				ui_password = connProps.getProperty("ui.password");
				jurisdiction = connProps.getProperty("jurisdiction");
				companyname = connProps.getProperty("companyname");
				qTitle = connProps.getProperty("question.title");
				fileName = connProps.getProperty("fileName");
				filePath = connProps.getProperty("filePath");
				driverpath = connProps.getProperty("driverpath");
				drivername = connProps.getProperty("drivername");
				// download=connProps.getProperty("downloads.path");
				// file_name=connProps.getProperty("fileName");
			}
		} catch (IOException e) {
			// Fallback to default property values
		}

	}

	/**
	 * The below function should be used with Wait<WebDriver> wait = new
	 * WebDriverWait(driver, 50);
	 *
	 * @param by
	 *
	 * @return ExpectedCondition
	 */
	public ExpectedCondition<WebElement> visibilityOfElementLocated(final By by) {
		return new ExpectedCondition<WebElement>() {
			public WebElement apply(final WebDriver driver) {
				final WebElement element = driver.findElement(by);

				return element.isDisplayed() ? element : null;
			}
		};
	}

	public String getServerUrl() {
		return server_url;
	}

	public String getQuestionTitle() {
		System.out.println("test qt : " + qTitle);
		return qTitle;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public String getDriverName() {
		return drivername;
	}

	public String getDriverPath() {
		return driverpath;
	}

	public String getDownloadsPath() {
		// TODO Auto-generated method stub
		return filePath;
	}

	public String getExcelFilePath() {
		return fileName;
	}

}
