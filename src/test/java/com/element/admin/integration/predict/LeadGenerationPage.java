package com.element.admin.integration.predict;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.element.admin.integration.common.Util;

public class LeadGenerationPage {
	public WebDriver driver;
	Util util;

	public int DRIVER_WAIT = 30; // 30 seconds

	/**
	 * Constructor.
	 *
	 * @param driver an instance of WebDriver
	 */
	public LeadGenerationPage(final WebDriver driver) {
		final ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, DRIVER_WAIT);
		PageFactory.initElements(finder, this);
		this.driver = driver;
		util = new Util();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
	}

	@FindBy(xpath = "//div[4]/div/div[2]/div[3]/div[2]/div/ul/li/button")
	public WebElement webelement_leadgeneraion;
	@FindBy(xpath = "//div[4]/div[2]/div[*]/div[*]")
	public WebElement webelement_Modules;
	@FindBy(xpath = "//div[4]/div[2]/div[1]/div[1]/h3")
	public WebElement webelement_GeneralCluster;
	@FindBy(xpath = "//div[4]/div[2]/div[1]/div[2]/h3")
	public WebElement webelement_dualCardHolders;
	@FindBy(xpath = "//div[4]/div[2]/div[1]/div[3]/h3")
	public WebElement webelement_predictingDualCard;
	@FindBy(xpath = "//div[4]/div[2]/div[2]/div[1]/h3")
	public WebElement webelement_earlyAdapters;
	@FindBy(xpath = "//div[4]/div[2]/div[2]/div[1]/ul/li/div")
	public WebElement webelement_gc_listofstories;
	@FindBy(xpath = "//div[4]/div[2]/div[2]/div[1]/ul/li[*]/button")
	public WebElement webelement_gc_createnewstorybtn;
	@FindBy(xpath = "//div[@id='storyNameDiv']/input")
	public WebElement webelement_gc_name;
	@FindBy(xpath = "//input[@id='typehead-inputbox']")
	public WebElement webelement_gc_linkOverflow;
	@FindBy(xpath = "//div[4]/div[4]/div/div[4]/div[2]")
	public WebElement webelement_gc_create_story;
	@FindBy(css = "div>div>svg > g > g:nth-child(1) > text:nth-child(4)")
	public WebElement webelement_gc_noname5Count;
	@FindBy(xpath = "//div[@id='mCSB_12_container']/li/a/div")
	public WebElement webelement_gc_genderCount;
	@FindBy(xpath = "//li[@id='Story-1180322095']/div/span")
	public WebElement webelement_gc_storyDelete;
	@FindBy(xpath = "//div[4]/div[3]/div/div/div[1]")
	public WebElement webelement_gc_deleteyes;
	@FindBy(xpath = "(//div[4]/div[2]/div[1])[3]")
	public WebElement webelement_gc_StoryDetails;
	@FindBy(xpath = "//li[@id='Story-892066338']/div")
	public WebElement webelement_gc_firstStory;
	@FindBy(xpath = "//a/h3[contains(.,'885')]")
	public WebElement webelement_gc_customerid885;
	@FindBy(xpath = "//div[4]/div[2]/div[2]/div[1]/div/div/div[1]/div/div[1]/h4")
	public WebElement webelement_gc_customerName;
	@FindBy(xpath = "//div[4]/div[1]/div[3]/div[1]/nav/ol/li[1]/a")
	public WebElement webelement_gc_cardHolderHome;
	// @FindBy(xpath="//div[@id='wrap']/div[2]/ul/li[1]/div[1]/strong")

	@FindBy(css = "div>svg>g")
	public WebElement webelement_dc_clustersCount;
	@FindBy(css = "div > svg > g > g:nth-child(1) > text:nth-child(3)")
	public WebElement webelement_dc_bstCluster;
	@FindBy(xpath = "//div[@id='idForToolTips']/div[3]/div")
	public WebElement webelement_dc_bstCustomerDetails;
	@FindBy(xpath = "//ul[@id='mTS_24_container']/li[1]/div[2]")
	public WebElement webelement_dc_bstAgeGroup;
	@FindBy(xpath = "//ul[@id='mTS_25_container']/li/a/span")
	public WebElement webelement_dc_bstAgeGroup68_72;
	@FindBy(css = "#ribbon23")
	public WebElement webelement_dc_bstRibbon;
	@FindBy(xpath = "//div[@id='ribbonInfo']/div[2]/div/h4/a")
	public WebElement webelement_dc_bstGoToConnectiondetails;
	@FindBy(xpath = "//div[@id='connectionInfoTooltip']/div[2]/div/div/div[1]/div/div/div[1]/div[1]")
	public WebElement webelement_dc_bstConnectionInformation;
	@FindBy(xpath = "//div[4]/div[2]/div[2]/div/div[1]/ul/li[1]/div[2]/strong")
	public WebElement webelement_pdc_totalCustomers;
	@FindBy(css = "div>div>div>div> svg > g > g:nth-child(1)")
	public WebElement webelement_pdc_uc3testing;
	@FindBy(xpath = "//div[4]/div[2]/div[2]/div/ul/li[1]/div[2]/strong")
	public WebElement webelement_pdc_uc3TotalCustomers;
	@FindBy(xpath = "//div[@id='mCSB_41_container']/tr[5]/td[1]/a/h3")
	public WebElement webelement_pdc_CustomerID326;
	@FindBy(xpath = "//div[4]/div[2]/div[3]/div[1]/div/table/thead/tr")
	public WebElement webelement_ea_TableHeaders;
	@FindBy(xpath = "//div[@id='slider-range']/span[1]")
	public WebElement webelement_ea_filterslider;
	@FindBy(xpath = "//div[@id='mCSB_44_container']/div/div/div[1]/div[1]/h3")
	public WebElement webelement_ea_topEarlyAdapters;
	@FindBy(xpath = "//div[@id='mCSB_44_container']/div/div/div[2]/div[1]/h3")
	public WebElement webelement_ea_topMerchantypes;
	@FindBy(xpath = "//div[@id='mCSB_45_container']/tr[21]/td[1]/a/h3")
	public WebElement webelement_ea_customerID;

	public void clickPredict() {
		webelement_leadgeneraion.click();
	}

	public int getLGModules() throws InterruptedException {
		return driver.findElements(By.xpath("//div[4]/div[2]/div[*]/div[*]/h3")).size();
	}

	public void clickGeneralCluster() {
		webelement_GeneralCluster.click();
	}

	public int getlistofStories() {
		return driver.findElements(By.xpath("//div[4]/div[2]/div[2]/div[1]/ul/li/div")).size();
	}

	public void clickCreatenewStory() {
		webelement_gc_createnewstorybtn.click();
	}

	public void typeAddStoryName() {
		webelement_gc_name.sendKeys("test a1234");
	}

	public void typeLinkOverflow() {
		webelement_gc_linkOverflow.sendKeys("demo_workflow");
		webelement_gc_linkOverflow.sendKeys(Keys.ARROW_DOWN);
		webelement_gc_linkOverflow.sendKeys(Keys.ENTER);
		webelement_gc_linkOverflow.sendKeys(Keys.RETURN);
	}

	public void clickCreateaStory() {
		webelement_gc_create_story.click();
	}

	public static boolean isDisplayed(WebElement element) {
		try {
			if (element.isDisplayed())
				return element.isDisplayed();
		} catch (NoSuchElementException ex) {
			return false;
		}
		return false;
	}

	public static boolean isClickable(WebElement element) {
		try {
			if (element.isEnabled())
				return element.isEnabled();
		} catch (NoSuchElementException ex) {
			return false;
		}
		return false;
	}

	public void clickElementWhenClickable(By locator, int timeout) {
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		element = wait.until(ExpectedConditions.elementToBeClickable(locator));
		element.click();
	}

	public int getComponentCount() {
		// return driver.findElements(By.cssSelector("rect:nth-child(25)")).size();
		WebElement svg = driver.findElement(By.cssSelector("svg"));
		List<WebElement> outertext = svg.findElements(By.cssSelector("g"));

		return outertext.size();
		/*
		 * for(WebElement texts : outertext) { String textcollection = texts.getText();
		 * if(textcollection.equals("xxxxxx")) { texts.click(); } }
		 */
	}

	public void clickComponentext() throws InterruptedException {
		Actions actions = new Actions(driver);
		loadingPrasent(webelement_gc_StoryDetails);
		System.out.println("get ready");
		/// Thread.sleep(5000);
		// webelement_gc_firstStory.click();
		actions.moveToElement(webelement_gc_firstStory).click().perform();
		loadingPrasent(webelement_gc_StoryDetails);
		actions.moveToElement(webelement_gc_noname5Count).click().build().perform();

	}

	public String getgendercount() throws InterruptedException {

		return webelement_gc_genderCount.getAttribute("innerText");
	}

	public void deleteStory() throws InterruptedException {
		loadingPrasent(webelement_gc_StoryDetails);
		webelement_gc_storyDelete.click();
		loadingPrasent(webelement_gc_StoryDetails);
		Thread.sleep(3000);
		webelement_gc_deleteyes.click();
		loadingPrasent(webelement_gc_StoryDetails);
	}

	public void loadingPrasent(WebElement welement) throws InterruptedException {
		while (!welement.getAttribute("class").contains("ng-hide")) {
			Thread.sleep(3000);
			System.out.println("Still Loading..");
		}
	}

	public void clickCustomerID885() {
		webelement_gc_customerid885.click();
	}

	public String getCustomerName() throws InterruptedException {
		// loadingPrasent(webelement_gc_customerName);
		while (!isDisplayed(webelement_gc_customerName)) {
			Thread.sleep(3000);
			System.out.println("customerName is not visible yet");
		}
		return webelement_gc_customerName.getText();
	}

	public void clickCardHolderHome() throws InterruptedException {
		while (!isDisplayed(webelement_gc_cardHolderHome)) {
			Thread.sleep(3000);
			System.out.println("cardHolderHome is not visible yet");
		}
		// ((JavascriptExecutor)
		// driver).executeScript("arguments[0].scrollIntoView(true);",
		// webelement_gc_cardHolderHome);
		Actions actions = new Actions(driver);
		actions.moveToElement(webelement_gc_cardHolderHome);
		actions.perform();
		// Thread.sleep(500);
		webelement_gc_cardHolderHome.click();
	}

	public void clickDualCardHolder() {
		webelement_dualCardHolders.click();
	}

	public int getDualCardClusters() {
		// return webelement_dc_clustersCount.getText();
		// return webelement_dc_clustersCount.findElements(By.cssSelector("g")).size();
		return driver.findElements(By.cssSelector("div >div >div >div > svg > g > g")).size();
	}

	public void clickBStCluster() {
		webelement_dc_bstCluster.click();
	}

	public String getTextBSTCustomerDeails() {
		return webelement_dc_bstCustomerDetails.getText();
	}

	public void clickBSTAgeGroup() {
		webelement_dc_bstAgeGroup.click();
	}

	public String getBSTAgeGroup68_72() {
		return webelement_dc_bstAgeGroup68_72.getText();
	}

	public void clickBSTRibbon() throws InterruptedException {
		Thread.sleep(3000);
		while (!isDisplayed(webelement_dc_bstRibbon)) {
			Thread.sleep(3000);
			System.out.println("customerName is not visible yet");
		}
		webelement_dc_bstRibbon.click();
	}

	public void clickBSTGoToConnectionDetails() {
		webelement_dc_bstGoToConnectiondetails.click();
	}

	public String getConnectionInformation() {
		return webelement_dc_bstConnectionInformation.getText();
	}

	public void clickPredictingDualCard() {
		webelement_predictingDualCard.click();
	}

	public String getPDCTotalCustomers() throws InterruptedException {
		Thread.sleep(3000);
		while (!isDisplayed(webelement_pdc_totalCustomers)) {
			Thread.sleep(3000);
			System.out.println("customerName is not visible yet");
		}
		return webelement_pdc_totalCustomers.getAttribute("innerText");
	}

	public void clickuc3Testing() {
		webelement_pdc_uc3testing.click();
	}

	public String getUC3TotalCustomers() {
		return webelement_pdc_uc3TotalCustomers.getAttribute("innerText");
	}

	public void clickPDCCustomerID() {
		webelement_pdc_CustomerID326.click();
	}

	public void clickEarlyAdapters() {
		webelement_earlyAdapters.click();
	}

	public String getEATableHeaders() {
		return webelement_ea_TableHeaders.getAttribute("innerText");
	}

	public void clickFilterByAverageSlider() throws InterruptedException {
		// while (!isClickable(webelement_gc_cardHolderHome))
		JavascriptExecutor jse = (JavascriptExecutor) driver;

		jse.executeScript("scroll(250, 0)");
		webelement_gc_cardHolderHome.click();
	}

	public String getTopEarlyAdaptersText() throws InterruptedException {
		Thread.sleep(3000);
		while (!isDisplayed(webelement_ea_topEarlyAdapters)) {
			Thread.sleep(3000);
			System.out.println("customerName is not visible yet");
		}
		return webelement_ea_topEarlyAdapters.getText();
	}

	public String getTopMerchantypesText() throws InterruptedException {
		Thread.sleep(3000);
		while (!isDisplayed(webelement_ea_topMerchantypes)) {
			Thread.sleep(3000);
			System.out.println("customerName is not visible yet");
		}
		return webelement_ea_topMerchantypes.getText();
	}

	public void clickEACustomerID() {
		Actions action = new Actions(driver);
		action.moveToElement(webelement_ea_customerID).click().perform();
		// webelement_ea_customerID.click();
	}
}
