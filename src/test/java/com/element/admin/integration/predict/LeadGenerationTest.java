package com.element.admin.integration.predict;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.openqa.selenium.WebDriver;
import com.element.admin.integration.aml.AmlPage;
import com.element.admin.integration.common.Login;

public class LeadGenerationTest {
	WebDriver driver;
	String actual = "", expected = "";

	@BeforeClass
	public void startup() {
		final Login login = new Login(driver);
		driver = login.setup();
	}

	@Test(priority = 1)
	public void domain() {
		final AmlPage aml = new AmlPage(driver);
		aml.clickAML();
	}

	@Test(priority = 2)
	public void TS_005_01() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickPredict();
		System.out.println("total module : " + lead.getLGModules());
		AssertJUnit.assertTrue(lead.getLGModules() == 4);
	}

	@Test(priority = 3)
	public void TS_005_02() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickGeneralCluster();
		System.out.println("total Stories list : " + lead.getlistofStories());
		lead.getComponentCount();
		System.out.println("Default Content is opened : " + (lead.getComponentCount() - 1));
	}

	@Test(priority = 4)
	public void TS_005_03() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickCreatenewStory();
		lead.typeAddStoryName();
		lead.typeLinkOverflow();
		lead.clickCreateaStory();
		lead.deleteStory();
	}

	@Test(priority = 5)
	public void TS_005_04() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickComponentext();
		System.out.println("" + lead.getgendercount());
		AssertJUnit.assertTrue(lead.getgendercount().contains("female: 247"));
	}

	@Test(priority = 6)
	public void TS_005_05() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickCustomerID885();
		System.out.println(lead.getCustomerName());
		AssertJUnit.assertTrue(lead.getCustomerName().contains("ZEPHANIA TODD"));
	}

	@Test(priority = 7)
	public void TS_005_06() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickCardHolderHome();
		lead.clickDualCardHolder();
		System.out.println(lead.getDualCardClusters());
		AssertJUnit.assertTrue(lead.getDualCardClusters() == 9);
	}

	@Test(priority = 8)
	public void TS_005_07() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickBStCluster();
		System.out.println(lead.getTextBSTCustomerDeails());
		AssertJUnit.assertTrue(lead.getTextBSTCustomerDeails().contains("Total Customers"));
	}

	@Test(priority = 9)
	public void TS_005_08() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickBSTAgeGroup();
		System.out.println(lead.getBSTAgeGroup68_72());
		AssertJUnit.assertTrue(lead.getBSTAgeGroup68_72().contains("AGE (68-72)"));
	}

	@Test(priority = 10)
	public void TS_005_09() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickBSTRibbon();
		lead.clickBSTGoToConnectionDetails();
		System.out.println(lead.getConnectionInformation());
		AssertJUnit.assertTrue(lead.getConnectionInformation().contains("RAMI LEVI CREDIT"));
	}

	@Test(priority = 11)
	public void TS_005_10() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickCardHolderHome();
		lead.clickPredictingDualCard();
		System.out.println(lead.getPDCTotalCustomers());
		AssertJUnit.assertTrue(lead.getPDCTotalCustomers().contains("1,000"));
	}

	@Test(priority = 12)
	public void TS_005_11() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickuc3Testing();
		System.out.println(lead.getUC3TotalCustomers());
		AssertJUnit.assertTrue(lead.getUC3TotalCustomers().contains("529"));
	}

	@Test(priority = 13)
	public void TS_005_12() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickPDCCustomerID();
		System.out.println(lead.getCustomerName());
		AssertJUnit.assertTrue(lead.getCustomerName().contains("STEWART GALLAGHER"));
	}

	@Test(priority = 14)
	public void TS_005_13() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickFilterByAverageSlider();
		lead.clickEarlyAdapters();
		// System.out.println("text : "+lead.getEATableHeaders());
		actual = lead.getEATableHeaders();
		expected = "CUSTOMER ID\n" + "EARLY ADAPTORS PURCHASES\n" + "PREFERRED CATEGORY";
		AssertJUnit.assertEquals(actual, expected);

		actual = lead.getTopEarlyAdaptersText();
		expected = "TOP EARLY ADOPTERS";
		AssertJUnit.assertEquals(actual, expected);

		actual = lead.getTopMerchantypesText();
		expected = "TOP MERCHANT TYPES";
		AssertJUnit.assertEquals(actual, expected);
	}

	@Test(priority = 15)
	public void TS_005_14() throws InterruptedException {
		final LeadGenerationPage lead = new LeadGenerationPage(driver);
		lead.clickEACustomerID();
		System.out.println(lead.getCustomerName());
		AssertJUnit.assertTrue(lead.getCustomerName().contains("ORSON FLEMING"));
	}

	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}
}
